@extends('master')
@section('sidebar')
    @include('sidebar')
@endsection

@section('content')


    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Order Manager</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-primary" href="{{ url('/purchase') }}"> Back</a>

            </div>

        </div>

    </div>


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>Whoops!</strong> There were some problems with your input.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif
            <table class="table table-bordered">
                    <tr>
                        <th style="width:30%">Purchase Number</th>
                        <td>{{$purchase->po_number}};</td>
                    </tr>
                
                    <tr>
                        <th style="width:30%">Customer</th>
                        <td>{{$purchase->customers->name}}</td>
                    </tr>
                    
                    <tr>
                        <th style="width:30%">Purchase Order Date</th>
                        <td>{{$purchase->date}}</td>
                    </tr>

                    <tr>
                        <th style="width:30%">Description</th>
                        <td>{{$purchase->description}}</td>
                    </tr>
                    
                    

            </table>

            <div class="form-group">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width:5%">No</th>
                            <th style="width:30%">Item</th>
                            <th style="width:10%">quantity</th>
                            <th style="width:10%">price</th>
                            <th style="width:30%">notes</th>
                        </tr>
                    </thead>
                    <tbody id="tbody-purchase_items">
                    
                    </tbody>

                </table>
            </div>

            <div class="form-group">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width:5%">No</th>
                            <th style="width:30%">Payment</th>
                            <th style="width:10%">note</th>
                            <th style="width:10%">Date</th>
                            
                        </tr>
                    </thead>

                        @if(count($finaces) == 0)
                        <tr>
                            <td colspan="5">There is no data.</td>
                        </tr>
                        @endif
                        @foreach ($finaces as $key => $finace)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $finace->payment }}</td>
                                <td>{{ $finace->note }}</td>
                                <td>{{ $finace->created_at }}</td>
                            </tr>
                        @endforeach

                </table>
            </div>

            <tr>
                <a class="btn btn-primary" href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#myModal">Add Payment</a>
            </tr>
    
            <tr>
                <a class="btn btn-success" href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#">Finish Order</a>
            </tr>
            
            
            <div class="form-group">
                <input type="hidden" name="purchase_items" id="purchase_items" value="{{ $purchase_items }}">
            </div>
          

@endsection

@section('content-modal')
<div class="modal fade" id="confirmEdit" tabindex="-1" role="dialog" aria-labelledby="confirmEditLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="confirmEditLabel">Confirm</h4>
            </div>
            <div class="modal-body">
                Are you sure want to save the changes?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button"  id="save_edit_ok" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Payment</h4>
      </div>
      <div class="modal-body">
        {!!
            Form::open([
                'role' => 'form',
                'url' => action('FinaceController@paymentupdate', [$purchase->id]),
                'enctype'=> 'multipart/form-data',
                'method' => 'post'
            ])
        !!}

        
        @include('form.text', [
                'field' => 'payment',
                'label' => 'Input Payment',
                'placeholder' => 'Input Paymnet',
                'default' => ''
            ])

        {!! Form::hidden('purchase_id',$purchase->id, ['class'=>'form-control']) !!}
            {!! $errors->first('purchase_id', '<p class="help-block">:message</p>') !!}

        @include('form.textarea', [
            'field' => 'note',
            'label' => 'Note',
            'placeholder' => 'Note',
            'attributes' => [
                'rows' => 3
            ],
            'default' => ''
        ])

        

            {!! Form::hidden('date',\Carbon\Carbon::now(), ['class'=>'form-control']) !!}
            {!! $errors->first('status', '<p class="help-block">:message</p>') !!}

       
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection


@section('content-js')
    @include('finace._form_item')
    <script>
    $(function () {
        var purchase_items = JSON.parse($('#purchase_items').val());


        function render() 
        {
            prepare();
            snapshot();

            $('#purchase_items').val(JSON.stringify(purchase_items));
            console.log(purchase_items);
            var tmpl = $('#formItemTmpl').html();
            Mustache.parse(tmpl);
            var data = { item : purchase_items };
            var html = Mustache.render(tmpl, data);
            $('#tbody-purchase_items').html(html);
            
            bind();
        }

        function snapshot()
        {
            
        }

        function bind()
        {
            //$('#AddItemButton').on('click', tambahItem);
            //$('.btn-delete-item').on('click', deleteItem);
            //$('.btn-edit-item').on('click', editItem);
            //$('.btn-save-item').on('click', saveItem);
            //$('.btn-cancel-item').on('click', cancelEdit);

            $('.input-qty').on('keyup');
            $('.input-item').on('keyup');
            $('.input-price').on('keyup');
            $('.input-note').on('keyup');

            $('.input-all').on('keyup');
            
        }

        function prepare() 
        {
            for (idx in purchase_items) {
                purchase_items[idx]['_id'] = idx;
                purchase_items[idx]['no'] = parseInt(idx) + 1;
            }
        }

        

         render();
    });
    </script>
@endsection




