<script type="x-tmpl-mustache" id="formItemTmpl">
	{% #item %}

		<tr>
			<td style="text-align:center;">{% no %}</td>
			
			<td>
				<div id="Item_{% _id %}" style="text-align:center;">{%item_name%}</div>
				<input type="text" style="text-align:center;" class="form-control input-sm hidden input-item input-edit" id="itemInput_{% _id %}" value="{%item_name%}" data-id="{% _id %}" data-row="">
			</td>

			<td>
				<div id="qty_{% _id %}" style="text-align:center;">{%qty%}</div>
				<input type="text" style="text-align:center;" class="form-control input-sm hidden input-qty input-edit" id="qtyInput_{% _id %}" value="{%qty%}" data-id="{% _id %}" data-row="">
			</td>

			<td style="text-align:right;">
				<div id="price_{% _id %}" >{%price%}</div>
				<input type="text" style="text-align:right;" class="form-control input-sm hidden input-price input-edit" id="priceInput_{% _id %}" value="{%price%}" data-id="{% _id %}" data-row="">
			</td>

			<td style="text-align:right;">
				<div id="note_{% _id %}" style="text-align:center;">{%note%}</div>
				<input type="textarea" style="text-align:right;" class="form-control input-sm hidden input-note input-edit" id="noteInput_{% _id %}" value="{%note%}" data-id="{% _id %}" data-row="">
			</td>

		</tr>
	{%/item%}

	
</script>