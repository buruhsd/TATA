@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    <table class="table table-bordered">
                    <tr>
                        <th style="width:30%">Purchase Number</th>
                        <td>{{$purchase->po_number}};</td>
                    </tr>
                
                    
                    

            </table>

                        <div class="form-group">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width:5%">No</th>
                            <th style="width:30%">Status</th>
                            <th style="width:10%">Message</th>
                            <th style="width:10%">Date</th>
                            <th style="width:50%">image</th>
                        </tr>
                    </thead>

                        @if(count($statuses) == 0)
                        <tr>
                            <td colspan="5">There is no data.</td>
                        </tr>
                        @endif
                        @foreach ($statuses as $key => $status)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $status->status_name }}</td>
                                <td>{{ $status->message }}</td>
                                <td>{{ $status->date }}</td>
                                <td> <img src="{{asset('img/'.$status->image)}}" width="250" height="250" alt="" class="img-responsive"></td>
                            </tr>
                        @endforeach

                </table>
            </div>
          

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
