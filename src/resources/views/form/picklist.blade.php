@if (isset($label))
<label class="control-label">{{ $label }}</label>
@endif

<div class="input-group {{ $errors->has($field) ? 'has-error' : '' }}">
	<span class="input-group-btn">
		<button class="btn btn-default" type="button" data-toggle="modal" 
			data-target="#{{ $name }}Modal" id="{{ $name }}Button"
			@if (isset($disabled) && $disabled) disabled @endif
		>
			<span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
		</button>
	</span>
	<input type="text" name="{{ $field }}" value="{{ isset($default) ? $default : '' }}"
		id="{{ $name }}Name" class="form-control" @if (isset($disabled) && $disabled) disabled @else readonly @endif {{ $placeholder ? $placeholder : '' }}
		data-toggle="modal" data-target="#{{ $name }}Modal"
	>

	@if (isset($delete) && $delete)
	<div class="input-group-btn">
		<button class="btn btn-default" type="button" id="{{ $name }}ButtonDel" title="Hapus">
			<span class="text-danger">
				<span class="glyphicon glyphicon-remove"></span>
			</span>
		</button>
	</div>
	@endif
</div>

<div class="has-error">
	@if ($errors->has($field))
	<span class="help-block text-danger">{{ $errors->first($field) }}</span>
	@endif
	@if ($errors->has($field . '_id'))
	<span class="help-block text-danger">{{ $errors->first($field . '_id') }}</span>
	@endif
</div>

{!! Form::hidden($field . '_id', isset($hidden_value) ? $hidden_value : null, ['id' => $name . 'Id']) !!}