@if (Session::has('UPDATE.OK'))
	<div class="alert alert-success">{{ _("Successfully saved") }}</div>
@elseif (Session::has('DELETE.OK'))
	<div class="alert alert-success">{{ _("successfully deleted") }}</div>
@elseif (Session::has('UPDATE.FAIL'))
	<div class="alert alert-danger">{{ _("Not successfully saved") }}</div>
@elseif (Session::has('DELETE.FAIL'))
	<div class="alert alert-danger">{{ _("not successfully deleted") }}</div>
@elseif (Session::has('UPD.BLOG.FAIL'))
	<div class="alert alert-danger">{{ _("Permalink has been used.") }}</div>
@elseif (Session::has('UPD.ACCTYPE.FAIL'))
	<div class="alert alert-danger">{{ _("Account type name has been used.") }}</div>
@elseif (Session::has('BOOK.INACTIVE'))
	<div class="alert alert-danger">{{ _("Current book is non active.") }}</div>
@elseif (Session::has('DELETE.DEACTIVATE'))
	<div class="alert alert-warning">
		{{ _("Data can not be deleted because it is already used. Status changed data becomes ") }}<B>{{ _("INACTIVE ") }}</B>.
	</div>
@elseif (Session::has('ERR.DATE'))
	<div class="alert alert-danger">{{ _("Input date and time invalid.") }}</div>
@endif