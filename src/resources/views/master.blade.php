<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>TATA APPS</title>

		<link rel="stylesheet" href="{{ asset(elixir("css/app.css")) }}">
		<noscript><meta http-equiv="refresh" content="1;url={{ action('HomeController@nojs') }}"></noscript>
		<style type="text/css">
			.datepicker{
				z-index: 9999;
			}
		</style>
		<link href="/css/jquery.dataTables.css" rel="stylesheet">
		<link href="/css/dataTables.bootstrap.css" rel="stylesheet">

		<script>
			var base_url = "{!! url('/') !!}";
		</script>
	</head>
	<body>
		<div class="container-fluid" style="">
			@include('navbar')

			<div class="page-wrapper">
				@if (isset(View::getSections()['sidebar']))
				<div class="sidebar-wrapper">
					<div class="sidebar">
						<!-- TODO: REMAKE THIS MENU TO UL -->
						<div class="list-group">
							@yield('sidebar')
						</div>
					</div>
				</div>
				<div class="content-wrapper">
					<div class="content">
						@include('top_notif')

						
						@yield('content')
					</div>
				</div>
				@else
				<div class="content-wrapper content-wrapper-no-sidebar">
					<div class="content">
						@include('top_notif')

						

						@yield('content')
					</div>
				</div>
				@endif

			</div>
			
			<footer>
				<div class="copyright">
					TATA by Durenworks &copy;
				</div>
			</footer>
			
			

			@yield('content-modal')
			
		</div><!-- end of container-fluid -->

		<script src="{{ asset(elixir("js/app.js")) }}"></script>
		<script type="text/javascript">
			var token = "{!! csrf_token() !!}";
		</script>
		

		@yield('content-js')
	</body>
</html>