@extends('master', ['active' => 'accounting'])

@section('sidebar')
	@include('accounting.account.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<a class="btn btn-primary" href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#myModal">{{ _("Add Account") }}</a>
		</div>
		<div class="row">
			<div class="content-header">
				<h2>
					{{ _("Account List") }}
				</h2>
			</div>
		</div>
		<div class="row">
			@if(count($accounts) == 0)
				<p>{{ _("There is no account yet.") }}</p>
			@endif
			<ol class="list-group list-group-root well sortable" id="accountsList">
				
			</ol>
			<input type="hidden" name="accounts" id="accounts_lists" value="">
		</div>
		{!!
			Form::open([
				'role' => 'form',
				'url' => action('Accounting\AccountController@update', $book),
				'method' => 'post',
				'id' => 'accountEditForm'
			])
		!!}
		{!! Form::close() !!}

	</div>
@endsection

@section('content-modal')
<div class="modal fade" id="confirmEdit" tabindex="-1" role="dialog" aria-labelledby="confirmEditLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="confirmEditLabel">{{ _("Confirm") }}</h4>
			</div>
			<div class="modal-body">
				{{ _("Are you sure want to save the changes?") }}
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ _("Cancel") }}</button>
				<button type="button"  id="save_edit_ok" class="btn btn-primary">{{ _("Save") }}</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="confirmMove" tabindex="-1" role="dialog" aria-labelledby="confirmMoveLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="confirmMoveLabel">{{ _("Confirm") }}</h4>
			</div>
			<div class="modal-body">
				{{ _("Are you sure want to move ") }}<b id="name_from"></b>{{ _(" and its child/children to ") }}<b id="name_to"></b>?
				<br>

				<div class="form-group">
					<label for="changeTo">{{ _("Change ") }}<b id="name_to2"></b>{{ _(" To?") }}</label>
					<select class="form-control" name="changeTo" id="changeTo"></select>
					<input type="hidden" name="changeFrom" id="changeFrom">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" id="move_cancel" class="btn btn-default" data-dismiss="modal">{{ _("Cancel") }}</button>
				<button type="button"  id="move_ok" class="btn btn-primary">{{ _("Move") }}</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">{{ _("Add Account") }}</h4>
      </div>
      <div class="modal-body">
        {!!
			Form::open([
				'role' => 'form',
				'url' => action('Accounting\AccountController@store', $book),
				'method' => 'post'
			])
		!!}

		@include('form.text', [
			'field' => 'name',
			'label' => 'Account Name',
			'placeholder' => 'Account Name',
			'default' => ''
		])

		@include('form.select', [
			'field' => 'account_type',
			'label' => 'Account Type',
			'options' => $acc_types,
			'default' => ''
		])

		@include('form.select', [
			'field' => 'sub_account',
			'label' => 'Sub Account of',
			'options' => $accs,
			'default' => ''
		])

		@include('form.text', [
			'field' => 'code',
			'label' => 'Account Number',
			'placeholder' => 'Account Number',
			'default' => ''
		])

		@include('form.textarea', [
			'field' => 'description',
			'label' => 'Description',
			'placeholder' => 'Description',
			'attributes' => [
				'rows' => 3
			],
			'default' => ''
		])

		@include('form.select', [
			'field' => 'is_cash',
			'label' => 'Is this cash account?',
			'options' => [
				'1' => 'Yes',
				'0' => 'No'
			],
			'default' => 0
		])

		@include('form.select', [
			'field' => 'status',
			'label' => 'Status',
			'options' => [
				'1' => 'Active',
				'0' => 'Inactive'
			],
			'default' => ''
		])
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{ _("Close") }}</button>
        <button type="submit" class="btn btn-primary">{{ _("Save") }}</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection

@section('content-js')
<script type="x-tmpl-mustache" id="changeto_template">
{% #accounts %}
	<option value="{% id %}">{% value %}</option>
{% /accounts %}
</script>
<script type="x-tmpl-mustache" id="edit_template">
	{% #account %}
	<h3 class="account-title-edit">Edit Account: {% account_name %}</h3>
	<div id="form_name_{% account_id %}" class="form-group form_name">
		<label class="control-label" for="name">{{ _("Account Name") }}</label>
		<input type="text" class="form-control" name="name" id="name_edit_{% account_id %}" placeholder="Account Name" value="{% account_name %}">
	</div>
	<div id="form_type_{% account_id %}" class="form-group form_type">
		<label class="control-label" for="name">{{ _("Account Type") }}</label>
		<select name="account_type" id="account_type_edit_{% account_id %}" class="form-control">
			{% #types %}
			<option value="{% type_id %}" {% t_selected %}>{% type_name %}</option>
			{% /types %}
		</select>
	</div>
	<div id="form_code_{% account_id %}" class="form-group form_code">
		<label class="control-label" for="code">Account Code</label>
		<input type="text" class="form-control" name="code" id="code_edit_{% account_id %}" placeholder="Account Number" value="{% account_code %}">
	</div>
	<div id="form_description_{% account_id %}" class="form-group form_description">
		<label class="control-label" for="description">{{ _("Description") }}</label>
		<textarea name="description" id="description_edit_{% account_id %}" class="form-control" rows="3">{% account_description %}</textarea>
	</div>
	<div  id="form_is_cash_{% account_id %}" class="form-group form_is_cash">
		<label class="control-label" for="status">Is this cash account?</label>
		<select name="is_cash" id="is_cash_edit_{% account_id %}" class="form-control">
			<option value="1" {% account_cash %}>{{ _("Yes") }}</option>
			<option value="0" {% account_no_cash %}>{{ _("No") }}</option>
		</select>
	</div>
	<div id="form_status_{% account_id %}" class="form-group form_status">
		<label class="control-label" for="status">Status</label>
		<select name="status" id="status_edit_{% account_id %}" class="form-control">
			<option value="1" {% account_active %}>{{ _("Active") }}</option>
			<option value="0" {% account_inactive %}>{{ _("Inactive") }}</option>
		</select>
	</div>
	<div class="form-group">
		<button type="button" class="btn btn-primary btn-save-edit" data-id="{% account_id %}">Submit</button>
		<button type="button" class="btn btn-danger btn-cancel-edit">Cancel</button>
	</div>
	{% /account %}
</script>

<script type="text/javascript">
	$(function(){
		render();

        $('ol.sortable').nestedSortable({
            handle: 'div',
            items: 'li',
            tolerance: 'pointer',
            toleranceElement: '> div',
            tabSize: 0,
            relocate: function(){
            	var dataAccounts = $.ajax({
				  url: '{!! action("Accounting\AccountController@getJSON", $book) !!}',
				  dataType: "json",
				  async: false,
				  success:function(result){
				  	return result;
				  }
				}).responseJSON;

            	serialized = $('ol.sortable').nestedSortable('serialize');
				$('#serializeOutput').text(serialized+'\n\n');

				arraied = $('ol.sortable').nestedSortable('toArray', {startDepthCount: 0});

            	var datax = $.ajax({
					url: "{!! action('Accounting\AccountController@sendDataChange', $book) !!}",
					method: "GET",
					data: { dataOri:dataAccounts, dataChange:arraied},
					success: function (data){
						data = JSON.parse(data);

						renderChangeTo(data[0].from_children);
						$("#name_from").text(data[0].name_from);
						$("#name_to").text(data[0].name_to);
						$("#name_to2").text(data[0].name_to);
						$("#changeFrom").val(data[0].id_to);
						$("#confirmMove").modal('show');
						$("#move_cancel").off("click").on("click", function(){
							$("#confirmMove").modal('hide');
							render();
						});

						$("#move_ok").off("click").on("click", function(){
							var change_from = $("#changeFrom").val();
							var change_to = $("#changeTo").val();
							var update = $.ajax({
								url: "{!! action('Accounting\AccountController@move', $book) !!}",
								method: "GET",
								data: { dataOri:dataAccounts, dataChange:arraied, change_from: change_from, change_to: change_to },
								success: function(msg){
									msg = JSON.parse(msg);
									$("#confirmMove").modal('hide');
									render();

									$.notify({
										title: '<strong>Notification!</strong>',
										message: msg.message
									},{
										type: msg.status
									});
								},
								error: function(msg){
									$.notify({
										title: '<strong>Notification!</strong>',
										message: msg
									},{
										type: 'danger'
									});
								}
							});
						});
					},
					error: function(msg){
						console.log('error');
					}
				});
            }

        });

        $("#deleteArea").droppable({
			drop: function( event, ui ) {
				console.log("dropped");
			}
		});
		
    });

    	function editAccount(i){
    		$('.edit_place').html('');
    		$('.data_place').show();
    		$('#data_place_' + i).hide();

    		var request = $.ajax({
				url: "{!! action('Accounting\AccountController@getItem', $book) !!}",
				method: "GET",
				dataType: "json",
				data: {id: i},
				success: function(msg){
					var tmpl = $('#edit_template').html();
					Mustache.parse(tmpl);
					var data = { account: msg[0] };
					var html = Mustache.render(tmpl, data);
					$('#edit_place_' + i).html(html);

					$(".btn-cancel-edit").off("click").on('click', render);
					$(".btn-save-edit").off("click").on('click', saveEditAccount);
				},
				error: function(msg){
					console.log('error');
				}
			});

			$('html, body').animate({
				scrollTop: $("#edit_place_" + i).offset().top
			}, 2000);
		}

		function saveEditAccount(){
			$('#confirmEdit').modal({
				keyboard: false,
				backdrop: false
			});
			$('#confirmEdit').modal('show');

			var i = $(this).data('id');

			$("#save_edit_ok").off("click").on('click', function(){
				var $form = $('#accountEditForm'),
					url = $form.attr( "action" ),
					token = {!! json_encode(Session::token()) !!};

		        var formData = {
		        	_token: token,
		        	id: i,
		            name: $("#name_edit_" + i).val(),
		            account_type: $("#account_type_edit_" + i).val(),
		            sub_account: $("#sub_account_edit_" + i).val(),
		            code: $("#code_edit_" + i).val(),
		            description: $("#description_edit_" + i).val(),
		            status: $("#status_edit_" + i).val(),
		            is_cash: $("#is_cash_edit_" + i).val(),
		        }

				if($("#name_edit_" + i).val() != '' && $("#code_edit_" + i).val() != ''){
					var request = $.ajax({
						url: url,
						method: "POST",
						dataType: "json",
						data: formData,
						success: function(msg){
							$.notify({
								title: '<strong>Notification!</strong>',
								message: msg.message
							},{
								type: msg.status
							});
							$('#confirmEdit').modal('hide');
							render();
						},
						error: function(msg){
							$.notify({
								title: '<strong>Notification!</strong>',
								message: msg
							},{
								type: 'danger'
							});
						}
					});
				}else{
					if($("#name_edit_" + i).val() == '' || typeof $("#name_edit_" + i).val() == 'undefined'){
						$("#form_name_" + i).addClass('has-error');
						$('#confirmEdit').modal('hide');
					}

					if($("#code_edit_" + i).val() == '' || typeof $("#code_edit_" + i).val() == 'undefined'){
						$("#form_code_" + i).addClass('has-error');
						$('#confirmEdit').modal('hide');
					}
				}
			});
		}

		function render(){
			var request = $.ajax({
				url: "{!! action('Accounting\AccountController@render_list', $book) !!}",
				method: "GET",
				dataType: "html"
			});
	 
			request.done(function( msg ) {
				$( "ol.sortable" ).html( msg );
			});
		}

		function changeDepth(){
			var request = $.ajax({
				url: "{!! action('Accounting\AccountController@render_list', $book) !!}",
				method: "GET",
				dataType: "html"
			});
	 
			request.done(function( msg ) {
				$( "ol.sortable" ).html( msg );
			});
		}

		function renderChangeTo(data) 
		{
			$('#changeTo').html("");
			data = JSON.parse(data);
			var tmpl = $('#changeto_template').html();
			Mustache.parse(tmpl);
			var html = Mustache.render(tmpl, { accounts : data });
			$('#changeTo').html(html);
		}
</script>
@endsection