@role([ 'marketing', 'admin'])
<a class="list-group-item {{ isset($active) && $active == 'index' ? 'active' : '' }}" href="{{ url('/customer') }}">
	Customer Database
</a>
@endrole

@role(['marketing', 'admin'])
<a class="list-group-item {{ isset($active) && $active == 'create' ? 'active' : '' }}" href="{{ url('/purchase') }}">
	Purchase Order
</a>
@endrole

@role(['marketing', 'admin'])
<a class="list-group-item {{ isset($active) && $active == 'index' ? 'active' : '' }}" href="{{ url('/manager') }}">
	Order Manager
</a>
@endrole

<a class="list-group-item {{ isset($active) && $active == 'edit' ? 'active' : '' }}" href="/settingss">
	Account Seeting
</a>

<a class="list-group-item {{ isset($active) && $active == 'edit' ? 'active' : '' }}" href="/log">
	Activty Log
</a>

@role(['payadmin', 'admin'])
<a class="list-group-item {{ isset($active) && $active == 'edit' ? 'active' : '' }}" href="/finace">
	Finace
</a>
@endrole

@role(['admin'])
<a class="list-group-item {{ isset($active) && $active == 'edit' ? 'active' : '' }}" href="/finace">
	Marketing Database
</a>
@endrole