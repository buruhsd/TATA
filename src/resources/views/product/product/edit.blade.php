@extends('product.master', ['active' => 'product'])

@section('sidebar')
	@include('product.product.sidebar', ['active' => 'edit'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="content-header">
				<h2>
					Edit Product
				</h2>
			</div>
		</div>
		<div class="row">
			{!!
				Form::open([
					'role' => 'form',
					'url' => action('Product\ProductController@update', $product->id),
					'method' => 'post'
				])
			!!}

			@include('form.text', [
				'field' => 'name',
				'label' => 'Name',
				'placeholder' => 'Name',
				'default' => $product->name
			])

			@include('form.text', [
				'field' => 'unit',
				'label' => 'Unit',
				'placeholder' => 'Unit',
				'default' => $product->unit
			])

			<div class="form-group">
				<label for="product_category" class="control-label">Product Category</label>
				<a href="#" class="btn btn-xs btn-success" style="margin-left: 10px;" data-toggle="modal" data-target="#categoryModal" data-backdrop="static" data-keyboard="false">Click for Add</a>
				<div class="control-input {{ $errors->has('product_category') ? 'has-error' : '' }}">
					<select class="form-control" id="categoryTarget" name="product_category"></select>
					@if ($errors->has('product_category'))
					<span class="help-block text-danger">{{ $errors->first('product_category') }}</span>
					@endif
				</div>
			</div>

			<div class="form-group">
				<label for="supplier" class="control-label">Supplier</label>
				<a href="#" class="btn btn-xs btn-success" style="margin-left: 10px;" data-toggle="modal" data-target="#supplierModal" data-backdrop="static" data-keyboard="false">Click for Add</a>
				<div class="control-input {{ $errors->has('supplier') ? 'has-error' : '' }}">
					<select class="form-control" id="supplierTarget" name="supplier"></select>
					@if ($errors->has('supplier'))
					<span class="help-block text-danger">{{ $errors->first('supplier') }}</span>
					@endif
				</div>
			</div>

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>

			{!! Form::close() !!}				
		</div>
	</div>
@endsection

@section('content-js')
<script type="x-tmpl-mustache" id="tplSelectCategory">
	{% #product_categories %}
		<option value="{% id %}" {% selected %}>{% name %}</option>
	{% /product_categories %}
</script>
<script type="x-tmpl-mustache" id="tplSelectSupplier">
	{% #suppliers %}
		<option value="{% id %}" {% selected %}>{% name %}</option>
	{% /suppliers %}
</script>
<script>
	render_categories();
	render_suppliers();

	function render_categories() {
		var id = {!! $product->id !!};
		var request = $.ajax({
			url: "{!! action('Product\ProductCategoryController@getAllData') !!}",
			method: "GET",
			data: {id: id},
			dataType: "json",
			success: function (msg) {
                var tmpl = $('#tplSelectCategory').html();

				Mustache.parse(tmpl);
				var data = { 
					product_categories: msg
				};
				//console.log(msg);
				var html = Mustache.render(tmpl, data);

				$('#categoryTarget').html(html);
            },
            error: function (data) {
                console.log('error');
            } 
		});
	}

	function render_suppliers() {
		var id = {!! $product->id !!};
		var request = $.ajax({
			url: "{!! action('Supplier\SupplierController@getAllData') !!}",
			data: {id: id},
			method: "GET",
			dataType: "json",
			success: function (msg) {
                var tmpl = $('#tplSelectSupplier').html();

				Mustache.parse(tmpl);
				var data = { 
					suppliers: msg
				};
				//console.log(msg);
				var html = Mustache.render(tmpl, data);

				$('#supplierTarget').html(html);
            },
            error: function (data) {
                console.log('error');
            } 
		});
	}

	function submitAddCategoryCallback (){
		var $form = $('#formAddCategory'),
			url = $form.attr( "action" ),
			token = {!! json_encode(Session::token()) !!};

        var formData = {
        	_token: token,
        	name: $('#categoryName').val(),
            description : $('#categoryDescription').val()
        }

        var request = $.ajax({
			url: url,
			method: "POST",
			data: formData,
			dataType: "json",
			success: function (data) {
                console.log('success');
                $( "#categoryName" ).val('');
                $( "#categoryDescription" ).val('');
                render_categories();
                $('#categoryModal').modal('hide');
            },
            error: function (data) {
                console.log('failed');
            } 
		});
	}

	function submitAddSupplierCallback (){
		var $form = $('#formAddSupplier'),
			url = $form.attr( "action" ),
			token = {!! json_encode(Session::token()) !!};

        var formData = {
        	_token: token,
        	name: $('#supplierName').val(),
            contact : $('#supplierContact').val(),
            phone : $('#supplierPhone').val(),
            address : $('#supplierAddress').val()
        }

        var request = $.ajax({
			url: url,
			method: "POST",
			data: formData,
			dataType: "json",
			success: function (data) {
                console.log('success');
                $('#supplierName').val(''),
	            $('#supplierContact').val(''),
	            $('#supplierPhone').val(''),
	            $('#supplierAddress').val('')
                render_suppliers();
                $('#supplierModal').modal('hide');
            },
            error: function (data) {
                console.log('failed');
            } 
		});
	}
</script>
@endsection

@section('content-modal')
<div class="modal fade" id="categoryModal" tabindex="-1" role="dialog" aria-labelledby="categoryModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Product Category</h4>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<div class="row">
						{!!
							Form::open([
								'role' => 'form',
								'url' => action('Product\ProductCategoryController@store_ajax'),
								'method' => 'post',
								'id' => 'formAddCategory'
							])
						!!}

						@include('form.text', [
							'field' => 'name',
							'label' => 'Name',
							'placeholder' => 'Product Category Name',
							'default' => '',
							'attributes' => [
								'id' => 'categoryName'
							]
						])

						@include('form.textarea', [
							'field' => 'description',
							'label' => 'Description',
							'placeholder' => 'Description',
							'default' => '',
							'attributes' => [
								'id' => 'categoryDescription'
							]
						])

						<div class="form-group">
							<button type="button" class="btn btn-primary" onclick="submitAddCategoryCallback()">Save</button>
						</div>

						{!! Form::close() !!}				
					</div>
				</div>
			</div>
			<div class="modal-footer">
				
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="supplierModal" tabindex="-1" role="dialog" aria-labelledby="supplierModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Supplier</h4>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<div class="row">
						{!!
							Form::open([
								'role' => 'form',
								'url' => action('Supplier\SupplierController@store_ajax'),
								'method' => 'post',
								'id' => 'formAddSupplier'
							])
						!!}

						@include('form.text', [
							'field' => 'name',
							'label' => 'Name',
							'placeholder' => 'Supplier Name',
							'default' => '',
							'attributes' => [
								'id' => 'supplierName'
							]
						])

						@include('form.text', [
							'field' => 'contact',
							'label' => 'Email',
							'placeholder' => 'Supplier Email',
							'default' => '',
							'attributes' => [
								'id' => 'supplierContact'
							]
						])

						@include('form.text', [
							'field' => 'phone',
							'label' => 'Phone Number',
							'placeholder' => 'Supplier Phone Number',
							'default' => '',
							'attributes' => [
								'id' => 'supplierPhone'
							]
						])

						@include('form.textarea', [
							'field' => 'address',
							'label' => 'Address',
							'placeholder' => 'Supplier Address',
							'default' => '',
							'attributes' => [
								'id' => 'supplierAddress'
							]
						])

						<div class="form-group">
							<button type="button" class="btn btn-primary" onclick="submitAddSupplierCallback()">Save</button>
						</div>

						{!! Form::close() !!}				
					</div>
				</div>
			</div>
			<div class="modal-footer">
				
			</div>
		</div>
	</div>
</div>
@endsection