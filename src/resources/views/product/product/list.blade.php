@extends('product.master', ['active' => 'product'])

@section('sidebar')
	@include('product.product.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			
		</div>
		<div class="row">
			<div class="table-responsive">
				<table class="table table-banner">
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Unit</th>
							<th>Category</th>
							<th>Supplier</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@if(count($products) == 0)
						<tr>
							<td colspan="5">There is no product.</td>
						</tr>
						@endif
						@foreach ($products as $key => $product)
							<tr>
								<td>{{ ++$key }}</td>
								<td>{{ $product->name }}</td>
								<td>{{ $product->unit }}</td>
								<td>{{ $product->categories->name }}</td>
								<td>{{ $product->suppliers->name }}</td>
								<td>
									<a href="{{ action('Product\ProductController@edit', [$product->id]) }}" class="btn btn-primary btn-xs">
										Edit
									</a>
									<a href="{{ action('Product\ProductController@delete', [$product->id]) }}" class="btn btn-danger btn-xs" onclick="confirm('Are you sure want to delete this data?')">
										Delete
									</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="pull-right">
				
			</div>
		</div>
	</div>
@endsection