@if(isset($active) && ($active == 'index' || $active == 'create'))
<a class="list-group-item {{ isset($active) && $active == 'index' ? 'active' : '' }}" href="{{ action('Product\ProductController@index') }}">
	Product List
</a>
<a class="list-group-item {{ isset($active) && $active == 'create' ? 'active' : '' }}" href="{{ action('Product\ProductController@create') }}">
	Create Product
</a>
@elseif(isset($active) && ($active == 'index' || $active == 'edit'))
<a class="list-group-item {{ isset($active) && $active == 'index' ? 'active' : '' }}" href="{{ action('Product\ProductController@index') }}">
	Product List
</a>
<a class="list-group-item {{ isset($active) && $active == 'edit' ? 'active' : '' }}" href="{{ action('Product\ProductController@edit', [$product->id]) }}">
	Edit Product
</a>
@endif