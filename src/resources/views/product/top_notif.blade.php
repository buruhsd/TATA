@if (Session::has('UPDATE.OK'))
	<div class="alert alert-success">Berhasil disimpan</div>
@elseif (Session::has('DELETE.OK'))
	<div class="alert alert-success">Berhasil dihapus</div>
@elseif (Session::has('UPDATE.FAIL'))
	<div class="alert alert-danger">Tidak berhasil disimpan</div>
@elseif (Session::has('DELETE.FAIL'))
	<div class="alert alert-danger">Tidak berhasil dihapus</div>
@elseif (Session::has('DELETE.DEACTIVATE'))
	<div class="alert alert-warning">
		Data tidak dapat dihapus karena sudah digunakan. Status data berubah menjadi <B>NONAKTIF</B>.
	</div>
@elseif (Session::has('ERR.DATE'))
	<div class="alert alert-danger">Input tanggal atau waktu invalid.</div>
@endif