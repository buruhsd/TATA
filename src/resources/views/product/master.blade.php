<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Transcalc | Product</title>

    <link rel="stylesheet" href="{{ asset(elixir("css/app.css")) }}">

  </head>
  <body>
    <div class="container-fluid" style="">
      @include('product.navbar')

      <div class="page-wrapper">
        @if (isset(View::getSections()['sidebar']))
        <div class="sidebar-wrapper">
          <div class="sidebar">
            <!-- TODO: REMAKE THIS MENU TO UL -->
            <div class="list-group">
              @yield('sidebar')
            </div>
          </div>
        </div>
        <div class="content-wrapper">
          <div class="content">
            @include('product.top_notif')
            @yield('content')
          </div>
        </div>
        @else
        <div class="content-wrapper content-wrapper-no-sidebar">
          <div class="content">
            @include('product.top_notif')
            @yield('content')
          </div>
        </div>
        @endif

      </div>
      
      
      <footer>
        <div class="copyright">
          Transcalc by Durenworks &copy;
        </div>
      </footer>

      @yield('content-modal')
      
    </div><!-- end of container-fluid -->

    <script src="{{ asset(elixir("js/app.js")) }}"></script>
    @yield('content-js')
  </body>
</html>