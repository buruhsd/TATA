@extends('product.master', ['active' => 'product_category'])

@section('sidebar')
	@include('product.product_category.sidebar', ['active' => 'create'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="content-header">
				<h2>
					Create Product Category
				</h2>
			</div>
		</div>
		<div class="row">
			{!!
				Form::open([
					'role' => 'form',
					'url' => action('Product\ProductCategoryController@store'),
					'method' => 'post'
				])
			!!}

			@include('form.text', [
				'field' => 'name',
				'label' => 'Name',
				'placeholder' => 'Product Category Name',
				'default' => ''
			])

			@include('form.textarea', [
				'field' => 'description',
				'label' => 'Description',
				'placeholder' => 'Description',
				'default' => ''
			])

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>

			{!! Form::close() !!}				
		</div>
	</div>
@endsection

@section('content-js')
@endsection