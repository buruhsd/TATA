@extends('product.master', ['active' => 'product_category'])

@section('sidebar')
	@include('product.product_category.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			
		</div>
		<div class="row">
			<div class="table-responsive">
				<table class="table table-banner">
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Description</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@if(count($product_categories) == 0)
						<tr>
							<td colspan="5">There is no product category.</td>
						</tr>
						@endif
						@foreach ($product_categories as $key => $product_category)
							<tr>
								<td>{{ ++$key }}</td>
								<td>{{ $product_category->name }}</td>
								<td>{{ $product_category->description }}</td>
								<td>
									<a href="{{ action('Product\ProductCategoryController@edit', [$product_category->id]) }}" class="btn btn-primary btn-xs">
										Edit
									</a>
									<a href="{{ action('Product\ProductCategoryController@delete', [$product_category->id]) }}" class="btn btn-danger btn-xs" onclick="confirm('Are you sure want to delete this data?')">
										Delete
									</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="pull-right">
				
			</div>
		</div>
	</div>
@endsection