@if(isset($active) && ($active == 'index' || $active == 'create'))
<a class="list-group-item {{ isset($active) && $active == 'index' ? 'active' : '' }}" href="{{ action('Product\ProductCategoryController@index') }}">
	Product Category List
</a>
<a class="list-group-item {{ isset($active) && $active == 'create' ? 'active' : '' }}" href="{{ action('Product\ProductCategoryController@create') }}">
	Create Product Category
</a>
@elseif(isset($active) && ($active == 'index' || $active == 'edit'))
<a class="list-group-item {{ isset($active) && $active == 'index' ? 'active' : '' }}" href="{{ action('Product\ProductCategoryController@index') }}">
	Product Category List
</a>
<a class="list-group-item {{ isset($active) && $active == 'edit' ? 'active' : '' }}" href="{{ action('Product\ProductCategoryController@edit', [$product_category->id]) }}">
	Edit Product Category
</a>
@endif