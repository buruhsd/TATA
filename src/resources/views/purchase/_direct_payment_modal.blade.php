<div class="modal fade form-horizontal" id="directPayModal" tabindex="-1" role="dialog" aria-labelledby="Payment Order" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Pembayaran</h4>
			</div>
			{!!
				Form::open(array(
					'role' => 'form',
					'method' => 'post',
					'id' => 'formPayment',
					'url' => action('Cashier\OrderController@store')
				))
			!!}
			<div class="modal-body">
				<div class="form-group">
					<label for="subtotal" class="col-xs-3 form-control-static">
						<h4>Subtotal</h4>
					</label>
					<div class="col-xs-9 form-control-static">
						<h4 id="labelSubtotal"></h4>
					</div>
				</div>
				<div class="form-group">
					<label for="discount" class="col-xs-3 form-control-static">Diskon</label>
					<div class="col-md-3">
						{!! Form::text('disc_amount', 0, [
								'class' => 'form-control',
								'id' => 'inputDisc'
							])	
						!!}
					</div>
					<div class="col-md-2">
						{!! Form::select('disc_type', [
							'PCT' => '%',
							'IDR' => 'IDR',
							], '', [
								'class' => 'form-control',
								'id' => 'inputDiscType',
							])	
						!!}
					</div>
					<label id="labelDisc" class="col-xs-4 form-control-static"></label>
				</div>
				<style type="text/css">
						.form-payment{
							padding-top: 0px;
							padding-bottom: 0px;
						}
						.form-payment2{
							padding-top: 7px;
							padding-bottom: 0px;
						}
					</style>
					<div class="form-group" style="margin-bottom: 0px">
						<label for="subtotal" class="col-xs-3 form-control-static form-payment2">
							<h5>Total Setelah Diskon</h5>
						</label>
						<div class="col-xs-9 form-control-static form-payment2">
							<h5 id="labelTotalDiskon"></h5>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 0px">
						<label for="subtotal" class="col-xs-3 form-control-static form-payment">
							<h5>Pembulatan</h5>
						</label>
						<div class="col-xs-9 form-control-static form-payment">
							<h5 id="labelRounding"></h5>
						</div>
					</div>
					<div class="form-group">
						<label for="subtotal" class="col-xs-3 form-control-static form-payment">
							<h5>Total Pembulatan</h5>
						</label>
						<div class="col-xs-9 form-control-static form-payment">
							<h5 id="labelGrand"></h5>
						</div>
					</div>
				<h3 class="form-group">
					<label for="total" class="col-xs-3 form-control-static">
						Total
					</label>
					<div class="col-xs-9 form-control-static">
						<strong>
							<span id="labelTotal"></span>
						</strong>
					</div>
					{!! Form::hidden('total', '', ['id' => 'totalPay']) !!}
				</h3>
				<div class="form-group">
					<label for="inputType" class="col-xs-3 form-control-static">Tipe</label>
					<div class="col-xs-9">
						{!! Form::select('pay_type', array(
							'CASH' => 'Tunai',
							'CARD' => 'Kartu Kredit/ Debit',
							'TRAN' => 'Transfer'
							), '', array(
							'class' => 'form-control',
							'id' => 'inputTypePayment',
						)) !!}
					</div>
				</div>
				<div class="form-group">
					<label for="inputCode" class="col-xs-3 form-control-static">Keterangan Pembayaran</label>
					<div class="col-xs-9">
						{!! Form::textarea('pay_desc', '', array(
							'class' => 'form-control',
							'rows' => 3
						)) !!}
					</div>
				</div>
				<div class="form-group">
					<label for="inputPayAmount" class="col-xs-3 form-control-static">Jumlah Bayar</label>
					<div class="col-xs-9" id="payment">
						{!! Form::text('pay_amount', '', array(
							'class' => 'form-control calculate',
							'id' => 'inputPayAmount',
							'placeholder' => 'Jumlah Bayar',
							'autofocus',
							'data-toggle' => 'tooltip',
							'data-placement' => 'bottom',
							'title' => 'Input pembayaran tidak boleh 0'
						)) !!}
					</div>
				</div>
				<h3 class="form-group">
					<label for="inputChange" id="labelChange" class="col-xs-3 form-control-static">Kembali</label>
					<div class="col-xs-9 form-control-static">
						<strong>
							<span id="inputChange"></span>
						</strong>
					</div>
				</h3>

				{!! Form::hidden('customer', '', ['id' => 'inputCustomer']) !!}
				{!! Form::hidden('customer_id', '', ['id' => 'inputCustomerId']) !!}
				{!! Form::hidden('items', json_encode([]), ['id' => 'inputItem']) !!}
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">Simpan</button>
				<button typ="button" class="btn btn-default" data-dismiss="modal">Batal</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>