@extends('master')

@section('sidebar')
    @include('sidebar')
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Add new purchase order</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ url('/purchase') }}"> Back</a>
            </div>
        </div>
    </div>


    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

             {!!
                Form::open([
                    'role' => 'form',
                    'url' => action('PurchasesController@store'),
                    'method' => 'post',
                ])
            !!}

            <div class="form-group">
            @include('form.text', [
                'field' => 'po_number',
                'label' => 'PO Number',
                'placeholder' => 'po number',
                'default' => ''
            ])
            </div>

            <div class="form-group">
             @include('form.picklist', [
                'field' => 'customer_id',
                'label' => 'Customer',
                'placeholder' => 'Customer',
                'name'  => 'customer',
            ])
            </div>
            
            <div class="form-group">
             @include('form.date', [
                'field' => 'date',
                'label' => 'Date',
                'placeholder' => 'Masukkan Tanggal Pembelian',
                'default' => \Carbon\Carbon::now()->format('d/m/Y'),
                'attributes' => [
                    'id' => 'date'
                ]
            ])
            </div>

            <div class="form-group">
            @include('form.textarea', [
                'field' => 'keterangan',
                'label' => 'Description',
                'placeholder' => 'Description',
                'default' => ''
            ])
            </div>
          
            {!! Form::hidden('status','Status PO', ['class'=>'form-control']) !!}
            {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                      
           <div class="form-group">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width:5%">No</th>
                            <th style="width:30%">Item</th>
                            <th style="width:10%">quantity</th>
                            <th style="width:10%">price</th>
                            <th style="width:30%">notes</th>
                            <th style="width:15%">Action</th>
                        </tr>
                    </thead>
                    <tbody id="tbody-purchase_items">
                    
                    </tbody>

                </table>
            </div>
            
            <div class="form-group">
                <input type="hidden" name="purchase_items" id="purchase_items" value="[]">
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
@endsection

@section('content-modal')   
    @include('purchase.modal_picklist', [
        'name' => 'customer',
        'title' => 'Daftar Customer',
        'placeholder' => 'Cari customer berdasarkan nama',
    ])
@endsection


@section('content-js')
    @include('purchase._form_item')
    <script>
    $(function () {
        CreatePicklist('customer', '/customer/list?');
    });
    </script>
    <script>
    $(function () {
        var purchase_items = JSON.parse($('#purchase_items').val());


        function render() 
        {
            prepare();
            snapshot();

            $('#purchase_items').val(JSON.stringify(purchase_items));
            console.log(purchase_items);
            var tmpl = $('#formItemTmpl').html();
            Mustache.parse(tmpl);
            var data = { item : purchase_items };
            var html = Mustache.render(tmpl, data);
            $('#tbody-purchase_items').html(html);
            
            bind();
        }

        function snapshot()
        {
            
        }

        function bind()
        {
            $('#AddItemButton').on('click', tambahItem);
            $('.btn-delete-item').on('click', deleteItem);
            $('.btn-edit-item').on('click', editItem);
            $('.btn-save-item').on('click', saveItem);
            $('.btn-cancel-item').on('click', cancelEdit);

            $('.input-qty').on('keyup');
            $('.input-item').on('keyup');
            $('.input-price').on('keyup');
            $('.input-note').on('keyup');

            $('.input-all').on('keyup');

            $('.input-new').keypress(function(e) {
                if (e.keyCode == 13) {
                    e.preventDefault();
                    tambahItem();
                }
            });

            $('.input-edit').keypress(function(e) {
                var i = $(this).data('id');

                if (e.keyCode == 13) {
                    e.preventDefault();
                    $('#simpan_' + i).click();
                }
            });
            
        }

        function prepare() 
        {
            for (idx in purchase_items) {
                purchase_items[idx]['_id'] = idx;
                purchase_items[idx]['no'] = parseInt(idx) + 1;
            }
        }

        function tambahItem() 
        {
            var item_name = $("#item_name").val();
            var qty = $("#qty").val();
            var price= $('#price').val();
            var note= $('#note').val();

            var input = {
                'item_name' : item_name,
                'qty' : qty,
                'price' : price,
                'note' : note
            };

            purchase_items.push(input);
            render();

        }

        function deleteItem() 
        {
            var i = parseInt($(this).data('id'), 10);
            
            purchase_items.splice(i, 1);
            render();
        }

        function editItem() 
        {

            var i = parseInt($(this).data('id'), 10);
            
            $('#Item_' + i).addClass('hidden');
            $('#qty_' + i).addClass('hidden');
            $('#price_' + i).addClass('hidden');
            $('#note_' + i).addClass('hidden');

            $('#edit_' + i).addClass('hidden');
            $('#delete_' + i).addClass('hidden');
            
            $('#itemInput_' + i).removeClass('hidden');
            $('#qtyInput_' + i).removeClass('hidden');
            $('#priceInput_' + i).removeClass('hidden');
            $('#noteInput_' + i).removeClass('hidden');
            

            $('#simpan_' + i).removeClass('hidden');
            $('#cancel_' + i).removeClass('hidden');

        }

        function cancelEdit()
        {

            var i = parseInt($(this).data('id'), 10);

            $('#item_' + i).removeClass('hidden');
            $('#qty_' + i).removeClass('hidden');
            $('#price_' + i).removeClass('hidden');
            $('#note_' + i).removeClass('hidden');
                       
            $('#edit_' + i).removeClass('hidden');
            $('#delete_' + i).removeClass('hidden');
            
            //$('#itemInputName_' + i).addClass('hidden');
            $('#itemInput_' + i).addClass('hidden');
            $('#qtyInput_' + i).addClass('hidden');
            $('#priceInput_' + i).addClass('hidden');
            $('#noteInput_' + i).addClass('hidden');           

            $('#simpan_' + i).addClass('hidden');
            $('#cancel_' + i).addClass('hidden');
        }

        function saveItem()
        {
            var i = parseInt($(this).data('id'), 10);

            var item_name = $('#itemInput_' + i).val();
            var qty = $("#qtyInput_" + i).val();
            var price= $('#priceInput_' + i).val();
            var note= $('#noteInput_' + i).val();

            purchase_items[i].item_name = item_name;
            purchase_items[i].qty = qty;
            purchase_items[i].price = price;
            purchase_items[i].note = note;

                      
            render();

        }


         render();
    });
    </script>
@endsection


