<script type="x-tmpl-mustache" id="purchase-item-table">
	{% #item %}
		<tr>
			<td style="text-align:center;">{% no %}</td>
			<td>
				<div id="item_{% _id %}" >{%item_name%}<div>
				<input type="text" class="form-control input-sm hidden" readonly id="itemInputName_{% _id %}" value="{%item_name%}" data-id="{% _id %}" data-row="">
				<input type="text" class="form-control input-sm hidden" readonly id="itemInputId_{% _id %}" value="{%item_id%}" data-id="{% _id %}" data-row="">
				<span class="text-danger hidden" id="errorInput_{% _id %}">
					Item dengan harga sama sudah terdapat pada tabel.
				</span>
			</td>
			<td style="text-align:right;">
				<div id="price_{% _id %}" >{% #formatMoney %}{% price %}{% /formatMoney %}</div>
				<input type="text" style="text-align:right;" class="form-control input-sm hidden input-price input-edit" id="priceInput_{% _id %}" value="{%price%}" data-id="{% _id %}" data-row="">
			</td>
			<td style="text-align:right;">	
				<div id="discount_{% _id %}">{% #formatMoney %}{%discount%}{% /formatMoney %}</div>
				<input type="text" style="text-align:right;" class="form-control input-sm hidden input-discount input-edit" id="discountInput_{% _id %}" value="{%discount%}" data-id="{% _id %}" data-row="">
			</td>
			<td>
				<div id="qty_{% _id %}" style="text-align:center;">{%qty%}</div>
				<input type="text" style="text-align:center;" class="form-control input-sm hidden input-qty input-edit" id="qtyInput_{% _id %}" value="{%qty%}" data-id="{% _id %}" data-row="">
			</td>
			
			<td style="text-align:right;">
				<div id="total_{% _id %}">{% #formatMoney %}{%total%}{% /formatMoney %}</div>
				<input type="text" style="text-align:right;" class="form-control input-sm hidden input-total input-edit" readonly="readonly" id="totalInput_{% _id %}" value="{%total%}" data-id="{% _id %}" data-row="" style="text-align:right;">
			</td>
			<td>
				<button type="button" id="edit_{% _id %}" class="btn btn-info btn-xs btn-edit-item" data-id="{% _id %}">ubah</button>
				<button type="button" id="delete_{% _id %}" class="btn btn-danger btn-xs btn-delete-item" data-id="{% _id %}">hapus</button>
				<button type="button" id="simpan_{% _id %}" class="btn btn-success btn-xs hidden btn-save-item" data-id="{% _id %}" >simpan</button>
				<button type="button" id="cancel_{% _id %}" class="btn btn-warning btn-xs hidden btn-cancel-item" data-id="{% _id %}" >batal</button>
			</td>
		</tr>
	{%/item%}

	<tr class="{{ $errors->has('purchase_items') || $errors->has('sale_items') ? 'has-error' : '' }}">
		<td colspan="2">
			<div style="width:100%">
				@include('form.text', [
					'field' => 'item',
					'name' => 'item',
					'placeholder' => 'Pilih Item'
				])
			</div>
			<span class="text-danger hidden" id="errorInput">
				Item dengan harga sama sudah terdapat pada tabel.
			</span>
			<span class="help-block text-danger 
				{{ $errors->has('purchase_items') || $errors->has('sale_items') ? '' : 'hidden' }}"
			>
				Isi detail item minimal 1.
			</span>
		</td>
		<td>
			<input type="text" id="harga" name="harga" style="text-align:right;" class="form-control input-item input-new" placeholder="Harga" value="0" >
		</td>
		<td>
			<input type="text" id="discount" name="discount" style="text-align:right;" class="form-control input-item input-new" placeholder="Diskon" value="0">
		</td>
		<td>
			<input type="text" id="qty" name="qty" style="text-align:center;" class="form-control input-item input-new" placeholder="Jumlah" value="0">
		</td>
		
		<td>
			<input type="text" id="total" readonly="readonly" style="text-align:right;" name="total" class="form-control input-new" placeholder="Total" style="text-align:right;">
		</td>
		<td>
			<button  type="button" class="btn btn-primary btn-sm" id="AddItemButton">Tambah</button>
		</td>
		
		
	</tr>
</script>