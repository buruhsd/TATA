<script type="x-tmpl-mustache" id="formItemTmpl">
	{% #item %}

		<tr>
			<td style="text-align:center;">{% no %}</td>
			
			<td>
				<div id="Item_{% _id %}" style="text-align:center;">{%item_name%}</div>
				<input type="text" style="text-align:center;" class="form-control input-sm hidden input-item input-edit" id="itemInput_{% _id %}" value="{%item_name%}" data-id="{% _id %}" data-row="">
			</td>

			<td>
				<div id="qty_{% _id %}" style="text-align:center;">{%qty%}</div>
				<input type="text" style="text-align:center;" class="form-control input-sm hidden input-qty input-edit" id="qtyInput_{% _id %}" value="{%qty%}" data-id="{% _id %}" data-row="">
			</td>

			<td style="text-align:right;">
				<div id="price_{% _id %}" >{%price%}</div>
				<input type="text" style="text-align:right;" class="form-control input-sm hidden input-price input-edit" id="priceInput_{% _id %}" value="{%price%}" data-id="{% _id %}" data-row="">
			</td>

			<td style="text-align:right;">
				<div id="note_{% _id %}" style="text-align:center;">{%note%}</div>
				<input type="textarea" style="text-align:right;" class="form-control input-sm hidden input-note input-edit" id="noteInput_{% _id %}" value="{%note%}" data-id="{% _id %}" data-row="">
			</td>

			<td>
				<button type="button" id="edit_{% _id %}" class="btn btn-info btn-xs btn-edit-item" data-id="{% _id %}">ubah</button>
				<button type="button" id="delete_{% _id %}" class="btn btn-danger btn-xs btn-delete-item" data-id="{% _id %}">hapus</button>
				<button type="button" id="simpan_{% _id %}" class="btn btn-success btn-xs hidden btn-save-item" data-id="{% _id %}" >simpan</button>
				<button type="button" id="cancel_{% _id %}" class="btn btn-warning btn-xs hidden btn-cancel-item" data-id="{% _id %}" >batal</button>
			</td>
		</tr>
	{%/item%}

	<tr class="{{ $errors->has('purchase_items') || $errors->has('sale_items') ? 'has-error' : '' }}">
		
		<td style="text-align:center;">{% no %}</td>

		<td>
			<input type="text" id="item_name" name="item_name" style="text-align:right;" class="form-control input-item input-new" placeholder="Item name" >
		</td>

		<td>
			<input type="text" id="qty" name="qty" style="text-align:center;" class="form-control input-item input-new" placeholder="Jumlah" value="0">
		</td>

		<td>
			<input type="text" id="price" name="price" style="text-align:right;" class="form-control input-item input-new" placeholder="price" value="0" >
		</td>

		<td>
			<input type="textarea" id="note" name="note" style="text-align:right;" class="form-control input-item input-new" placeholder="please describe what you want" >
		</td>
		
		<td>
			<button  type="button" class="btn btn-primary btn-sm" id="AddItemButton">Tambah</button>
		</td>
		
		
	</tr>
</script>