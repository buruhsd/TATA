<div class="modal fade" id="{{ $name }}Modal">
  	<div class="modal-dialog">
		<div class="modal-content">
		  
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title">{{ $title }}</h4>
		  </div>
		  
		  <div class="modal-body">
		  	<div class="shade-screen">
		  		<div class="shade-screen hidden">
		  			<img src="/image/ajax-loader.gif">
		  		</div>
		  	</div>
		  	<div class="form-inline form-search">
		  		<input type="text" id="{{ $name }}Search" class="form-control" style="width:88%" placeholder="{{ $placeholder }}">
		  		<button class="btn btn-warning btn-sm" type="button" id="{{ $name }}ButtonSrc">Cari</button>
		  	</div>
			
			<div id="{{ $name }}Table"></div>			
		  
		  </div>
		  
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>				
		  </div>
		</div>
 	</div>
</div>