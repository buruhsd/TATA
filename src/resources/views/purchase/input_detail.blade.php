    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>Whoops!</strong> There were some problems with your input.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif


             {!!
                Form::open([
                    'role' => 'form',
                    'url' => action('PurchasesController@store'),
                    'method' => 'post',
                ])
            !!}

            @include('form.text', [
                'field' => 'po_number',
                'label' => 'po number',
                'placeholder' => 'po number',
                'default' => ''
            ])

            {!! Form::label('customer_id', 'Customer', ['class'=>'col-md-2 control-label']) !!}
             <div class="col-md-4">
            {!! Form::select('customer_id', [''=>'']+App\Models\Customer::pluck('name','id')->all(), null, [
            'class'=>'js-selectize',
            'placeholder' => 'Pilih']) !!}
              {!! $errors->first('customer_id', '<p class="help-block">:message</p>') !!}
             </div>
            
          
            {!! Form::hidden('status','1', ['class'=>'form-control']) !!}
            {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
          
            


            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>

            {!! Form::close() !!}