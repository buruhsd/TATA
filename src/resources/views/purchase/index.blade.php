@extends('master')
@section('sidebar')
    @include('sidebar')
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
           
        <div class="row">
            <div class="table-responsive">
                <table class="table table-banner">
                 <div class="pull-right">

                <a class="btn btn-success" href="{{ url('purchase/add') }}"> Add New PO </a>

            </div>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>nomer PO</th>
                            <th>Nama Customer</th>
                            <th>status</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($purchases) == 0)
                        <tr>
                            <td colspan="5">There is no data.</td>
                        </tr>
                        @endif
                        @foreach ($purchases as $key => $purchase)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $purchase->po_number }}</td>
                                <td>{{ $purchase->customers->name }}</td>
                                <td>{{ $purchase->status }}</td>
                               
                                <td>
                                    <a class="btn btn-primary" href="{{ action('PurchasesController@edit', $purchase->id) }}">Edit</a>
                                    <a class="btn btn-primary" href="#">delete</a>
                                    
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="pull-right">
                
            </div>
        </div>
    </div>
@endsection
