@extends('cashier.layout')

@section('sidebar')
	@include('cashier.order_create_sidebar')
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="detail">
				<div class="form-group">
					@include('form.picklist', [
						'label' => 'Pelanggan',
						'field' => 'customer',
						'name' => 'customer',
						'placeholder' => 'Pilih pelanggan',
						'delete' => 'true'
					])
				</div>

				<h4 class="hidden" id="editTitle"></h4>

				<div class="form-group">
					@include('form.picklist', [
						'label' => 'Produk',
						'field' => 'product',
						'name' => 'product',
						'placeholder' => 'Pilih Produk'
					])
				</div>

				<div class="option-box hidden" id="listOptions">
					<dl class="dl-horizontal custom-dl" id="options"></dl>
				</div>

				<div class="row">
					<div class="col-md-4">
						@include('form.select', [
							'field' => 'side',
							'label' => 'Sisi',
							'options' => [],
							'attributes' => [
								'id' => 'side',
								'disabled' => 'disabled'
							]
						])
					</div>

					<div class="col-md-4">
						@include('form.text', [
							'field' => 'page',
							'label' => 'Lembar/set',
							'placeholder' => 'Lembar/set',
							'attributes' => [
								'id' => 'page'
							]
						])
					</div>

					<div class="col-md-4">
						@include('form.text', [
							'field' => 'copy',
							'label' => 'Copy',
							'placeholder' => 'Copy',
							'attributes' => [
								'id' => 'copy'
							]
						])
					</div>
				</div>

				<div class="form-group">
					@include('form.text', [
						'label' => 'Nama File',
						'field' => 'filename',
						'placeholder' => 'Masukkan Nama File',
						'attributes' => [
								'id' => 'fileName'
							]
					])
				</div>

				<div class="form-group">
					<button type="button" class="btn btn-primary btn-sm btn-submit pull-right" id="AddItemButton">
						Submit
					</button>
					<div class="edit-state-button pull-right hidden" data-id="">
						<button type="button" class="btn btn-success btn-sm" id="SaveItemButton" data-id="">
							Simpan
						</button>
						<button type="button" class="btn btn-warning btn-sm" id="CancelItemButton" data-id="">
							Batal
						</button>
					</div>
				</div>
			</div>
		</div>

		<div id="shadeLoading" class="shade-screen hidden">
			<img src="/image/ajax-loader.gif">
		</div>
		<div class="row hidden" id="listItem">
			<div class="form-group">
				<label for="itemOrder">
					Item
				</label>
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<th>#</th>
							<th width="45%" colspan="2">Produk</th>
							<th>Nama File</th>
							<th>Qty</th>
							<th>Harga</th>
							<th>Diskon</th>
							<th>Subtotal</th>
							<th width="12%">Aksi</th>
						</thead>
						<tbody id="itemTable"></tbody>
						<tfoot>
							<tr>
								<th colspan="6" class="text-right">
									<h4><b>Total</b></h4>
								</th>
								<th>
									<h4 class="text-right" id="subtotal" data-total=""></h4>
								</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>

			<button class="btn btn-success" type="button" id="PayButton" data-toggle="modal" data-target="#directPayModal">
				Bayar
			</button>
		</div>

		{!! Form::hidden('sides', json_encode($sides), ['id' => 'selectSide']) !!}
		{!! Form::hidden('options', json_encode($options), ['id' => 'selectOption']) !!}
		{!! Form::hidden('count', 0, ['id' => 'countPay']) !!}
	</div>
@endsection

@section('content-modal')
	@include('cashier._direct_payment_modal')
	
	@include('cashier.modal_picklist', [
		'name' => 'product',
		'title' => 'Daftar Produk',
		'placeholder' => 'Cari produk berdasarkan nama',
	])

	@include('cashier.modal_picklist', [
		'name' => 'customer',
		'title' => 'Daftar Customer',
		'placeholder' => 'Cari customer berdasarkan nama',
	])
@endsection

@section('content-js')
	@include('cashier._item_list')
	@include('_order_item_select')

	<script src="{{ elixir("js/cashier/order_payment.js") }}"></script>
@endsection