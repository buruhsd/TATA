@extends('master')
@section('content')


             <table class="table table-bordered">
                    <tr>
                        <th style="width:30%">Purchase Number</th>
                        <td>{{$purchase->po_number}};</td>
                    </tr>
                
                    <tr>
                        <th style="width:30%">Customer</th>
                        <td>{{$purchase->customers->name}}</td>
                    </tr>
                    
                    <tr>
                        <th style="width:30%">Purchase Order Date</th>
                        <td>{{$purchase->date}}</td>
                    </tr>

                    <tr>
                        <th style="width:30%">Description</th>
                        <td>{{$purchase->description}}</td>
                    </tr>
                    
                    

            </table>          

@endsection