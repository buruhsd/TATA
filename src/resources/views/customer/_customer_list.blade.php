<table class="table">
	<thead>
	  <tr>
		<th>Nama</th>
		<th>Pilih</th>
	  </tr>
	</thead>
	
	<tbody>
		@foreach($lists as  $list)
			<tr>
				<td>
					{{ $list->id }}
					{{ $list->name }}
				</td>
				<td>
					<button data-dismiss="modal" class="btn btn-warning btn-xs btn-choose" 
						type="button" data-id="{{ $list->id }}" data-name="{{ $list->name }}">Pilih</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}