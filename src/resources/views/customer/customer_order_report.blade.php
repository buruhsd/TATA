@extends('admin.layout')

@section('content')
	
	<div class="container-fluid">
		{!!
			Form::open(array(
				'class' => 'form-signin',
				'role' => 'form',
				'url' => action('Admin\OrderController@customerOrderReport'),
				'method' => 'get',
			))
		!!}
		<div class="row form-group">
			<div class="col-md-6">
				<div class="row form-group">
					<label class="control-label">
						Produk
					</label>
					<div class="row form-group">
					@foreach($products as $product)
						<div class="col-md-6">
							{!! Form::checkbox('pro[]', $product->id, (in_array($product->id, $pros)) ? 1 : 0, [
								'id' => 'cb'.$product->id,
							]) !!}
							{{$product->name}}
						</div>
					@endforeach
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row form-group">
					<div class="col-md-6">
						@include('form.date', [
							'field' => 'from_date',
							'label' => 'Tanggal Mulai',
							'default' => $from_date->format('d/m/Y')
						])
					</div>		
					<div class="col-md-6">
						@include('form.date', [
							'field' => 'to_date',
							'label' => 'Tanggal Akhir',
							'default' => $to_date->format('d/m/Y')
						])
					</div>
				</div>
				<div class="row form-group">
					<div class="col-md-6">
						@include('form.select', [
							'label' => 'Tipe Jumlah',
							'field' => 'type',
							'options' => [
								'unit' => 'Unit',
								'rupiah' => 'Rupiah'
							],
							'default' => $type
						])
					</div>
					<div class="col-md-6">
						@include('form.select', [
							'label' => 'Member',
							'field' => 'member',
							'options' => [
								'1' => 'Member',
								'2' => 'Non Member',
								'0' => 'Semua'
							],
							'default' => $member
						])
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12" align="right">
				<button type="submit" class="btn btn-primary">Filter</button>
				<a class="btn btn-success"
					href="{{ action('Admin\OrderController@exportCustomerOrderReport', ['from_date' => $from_date->format('d/m/Y'), 'to_date' => $to_date->format('d/m/Y'), 'type' => $type, 'member' => $member, 'pro' => json_encode($pros), 'checked' => $checked 
					]) }}" 						
				>
					Print to Excel
				</a>
			</div>
		</div>
	{!! Form::close() !!}

	<div class="row">
		<div class="title text-center">
			<h2>
				Laporan Agregat Order Pelanggan<br>
				<small>
					Tanggal: {{i18l_date($from_date, ['date' => IntlDateFormatter::LONG, 'time' => IntlDateFormatter::NONE])}} s/d {{i18l_date($to_date, ['date' => IntlDateFormatter::LONG, 'time' => IntlDateFormatter::NONE])}}
				</small>
				<br>
			</h2>
		</div>
		<div class="table-responsive">
			<table class="tabelku" style=" margin-left: auto; margin-right: auto;">
				<thead>
					<tr>
						@if($orders)
							<th style="text-align:center" width="35px">#</th>
							<th style="text-align:center; min-width:150px">Nama Pelanggan</th>
							@foreach($checked as $key => $check)
								<th style="text-align:center; min-width:100px">{{$check->name}}</th>
							@endforeach
							<th style="text-align:center; min-width:150px">Total</th>
						@endif
					</tr>
				</thead>
				<tbody>
					@if($orders)

					<?php
						$bool = [];
						foreach($orders[0] as $key => $o){
							$bool[$key] = false;
							foreach ($orders as $index => $value) {
								if($orders[$index][$key]->qty != null)
									$a = true;
								else
									$a = false;
								
								$bool[$key] = $bool[$key] || $a;
							}
						}
						//var_dump($bool);
						$no = 1;
					?>

					@foreach($orders[0] as $key => $order)
						@if($bool[$key])
						<?php $total = 0; ?>
						<tr>
							<td align="center">{{$no++}}</td>
							<td>{{$order->full_name}}</td>
							@foreach($orders as $index => $order)
								
								<td align="@if($type == "unit") center @else right @endif">
									@if($type == "unit")
										@if($orders[$index][$key]->qty)
											{{ i18l_number($orders[$index][$key]->qty) }}
										@else
											{{ i18l_number(0) }}
										@endif
										<?php $total = $total + $orders[$index][$key]->qty;?>
									@elseif($type == "rupiah")
										@if($orders[$index][$key]->total)
											{{ i18l_number($orders[$index][$key]->total) }}
										@else
											{{ i18l_number(0) }}
										@endif
										<?php $total = $total + $orders[$index][$key]->total;?>
									@endif
								</td>

							@endforeach
							<td align="@if($type == "unit") center @else right @endif">
								{{ i18l_number($total) }}
							</td>
						</tr>
						@endif
					@endforeach
					
					
					@endif
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection
@section('content-js')
	<script>

		$("#checkbox").attr("checked", true);
	</script>
@endsection