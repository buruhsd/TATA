@extends('admin.layout')

@section('content')
	<div class="container-fluid">
		<div class="row form-group" style="margin-bottom : 20px">
			{!!
				Form::open(array(
					'class' => 'form-signin',
					'role' => 'form',
					'url' => action('Admin\CustomerController@report'),
					'method' => 'get',
				))
			!!}
			
			<div class="col-md-2" style="margin-bottom : 10px">
				@include('form.select', [
					'field' => 'state',
					'label' => 'Status Aktif',
					'default' => $state,
					'options' => [
						'all' => 'Semua',
						'active' => 'Aktif',
						'inactive' => 'Non Aktif'
					]
				])
			</div>
			<div class="col-md-2">
				@include('form.select', [
					'field' => 'member',
					'label' => 'Status Member',
					'default' => $member,
					'options' => [
						'all' => 'Semua',
						'member' => 'Member',
						'non-member' => 'Bukan Member'
					]
				])
			</div>
			{!! Form::hidden('sort', $sort, array('id' => 'sort')) !!}
			<div class="form-group col-md-4" style="margin-top : 25px">
				<button type="submit" class="btn btn-primary">Filter</button>
				<a class="btn btn-success"
					href="{{ action('Admin\CustomerController@export', [
						'state' => $state,
						'member' => $member,
						'sort' => $sort
					]) }}" 						
				>
					Print to Excel
				</a>
			</div>
			{!! Form::close() !!}
		</div>
		<?php
			$sort_array = [ 
				'name' => 'name',
				'no_member' => 'no_member',
				'email' => 'email',
				'state' => 'state'
			];

			if($sort){
				$index = str_replace('-', '', $sort);
				
				if($sort==$sort_array[$index])
					$sort_array[$index] = '-'.$sort_array[$index];

				else if($sort=="-".$sort_array[$index])
					$sort_array[$index]='';
			}
				
		?>
		<div class="row">
			<div class="title text-center">
				<h2>
					Laporan Pelanggan
				</h2>
			</div>
			<div class="table-responsive">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>#</th>
							<th><a href="{{ action('Admin\CustomerController@report', [
									'state' => $state,
									'member' => $member,
									'sort' => $sort_array['name']
								]) }}"> 
								Nama Customer
								 <span class="glyphicon {{ sort_icon_class($sort, 'name') }}" aria-hidden="true"></span>
								</a>
							</th>
							<th><a href="{{ action('Admin\CustomerController@report', [
									'state' => $state,
									'member' => $member,
									'sort' => $sort_array['no_member']
								]) }}">
								Nomor Member
								<span class="glyphicon {{ sort_icon_class($sort, 'no_member') }}" aria-hidden="true"></span></a>
								</a>
							</th>
							<th><a href="{{ action('Admin\CustomerController@report', [
									'state' => $state,
									'member' => $member,
									'sort' => $sort_array['email']
								]) }}"> Email
								<span class="glyphicon {{ sort_icon_class($sort, 'email') }}" aria-hidden="true"></span></a>
								</a>
							</th>
							<th>Alamat</th>
							<th>Telepon</th>
							<th>
								<a href="{{ action('Admin\CustomerController@report', [
									'state' => $state,
									'member' => $member,
									'sort' => $sort_array['state']
								]) }}">Status
								<span class="glyphicon {{ sort_icon_class($sort, 'state') }}" aria-hidden="true"></span></a>
								</a>
							</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($customers as $key => $customer)
							<tr>
								<td>{{ $key + 1 }}</td>
								<td>{{ $customer->full_name }}</td>
								<td>{{ $customer->member_number ? $customer->member_number : '-' }}</td>
								<td>{{ $customer->email ? $customer->email : '-' }}</td>
								<td>{{ $customer->address ? $customer->address : '-' }}</td>
								<td>{{ $customer->phone }}</td>
								<td>
									@if ($customer->is_active)
										Aktif
									@else
										Non Aktif
									@endif
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="pull-right">
				{!! $customers->appends(Request::except('page'))->render() !!}
			</div>
		</div>
	</div>
@endsection