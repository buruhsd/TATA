@extends('master')

@section('sidebar')
    @include('sidebar')
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
           
        <div class="row">
            <div class="table-responsive">
                <table class="table table-banner">
                <div class="pull-right">

                <a class="btn btn-success" href="{{ url('customer/add') }}"> Add New Customer </a>

            </div>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Company</th>
                            <th>Country</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($customers) == 0)
                        <tr>
                            <td colspan="5">There is no data.</td>
                        </tr>
                        @endif
                        @foreach ($customers as $key => $customer)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $customer->name }}</td>
                                <td>{{ $customer->company }}</td>
                                <td>{{ $customer->country }}</td>
                                <td>
                                    <a class="btn btn-primary" href="{{ action('CustomerController@edit', $customer->id) }}">Edit</a>
                                    <a class="btn btn-primary" href="{{ action('CustomerController@show', $customer->id) }}">show</a>
                                    <a class="btn btn-danger" href="#">delete</a>
                                    
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="pull-right">
                
            </div>
        </div>
    </div>
@endsection
