@extends('master')
@section('sidebar')
    @include('sidebar')
@endsection

@section('content')


    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Add new customer</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-primary" href="{{ url('/customer') }}"> Back</a>

            </div>

        </div>

    </div>


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>Whoops!</strong> There were some problems with your input.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif


    {!!
                Form::open([
                    'role' => 'form',
                    'url' => action('CustomerController@store'),
                    'method' => 'post'
                ])
            !!}

            @include('form.text', [
                'field' => 'name',
                'label' => 'Name',
                'placeholder' => 'Name',
                'default' => ''
            ])

            @include('form.text', [
                'field' => 'email',
                'label' => 'Email',
                'placeholder' => 'Email',
                'default' => ''
            ])

            @include('form.password', [
                'field' => 'password',
                'label' => 'password',
                'placeholder' => 'Password',
                'default' => ''
            ])            

            @include('form.text', [
                'field' => 'company',
                'label' => 'Company',
                'placeholder' => 'Company',
                'default' => ''
            ])

            @include('form.text', [
                'field' => 'country',
                'label' => 'Country',
                'placeholder' => 'Country',
                'default' => ''
            ])



            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>

            {!! Form::close() !!}


@endsection