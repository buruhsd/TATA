@extends('admin.layout')

@section('sidebar')
	@include('admin.customer_sidebar', ['active' => 'index'])
@endsection

@section('content')

	<div class="container-fluid">
		<div class="row">
			{!!
				Form::open(array(
					'role' => 'form',
					'url' => action('Admin\CustomerController@index'),
					'method' => 'get',
					'id' => 'form_filter'
				))
			!!}
			<div class="row">
				<div class="col-md-2">
					<label>Status Aktif</label>
					{!! 
					    	Form::select('active', [
					    		'all' => 'Semua',
					    		true => 'Aktif',
					    		false => 'Non Aktif'	
					    	], $active, [ 
					    		'class' => 'form-control'
					]) !!}
				</div>
				<div class="col-md-2">
					<label>Tipe Pelanggan</label>
					{!! 
					    	Form::select('member', [
					    		'all' => 'Semua',
							'member' => 'Member',
							'non-member' => 'Bukan Member'
					    	], $member, [ 
					    		'class' => 'form-control'
					]) !!}
				</div>
				<div class="col-md-6">
					<label>Pencarian Detail</label>
					<div class="input-group input-search">
				      		{!! Form::text('q', $q, [
							'class' => 'form-control',
							'placeholder' => 'Cari data pelanggan berdasarkan nama, email, alamat, telepon',
							'autofocus' => 'true'
						]) !!}
					      	<div class="input-group-addon">
					      		<span class="glyphicon glyphicon-search"></span>
					      	</div>
				    </div>
				</div>
				{!! Form::hidden('sort', $sort) !!}
				<div class="col-md-2" style="margin-top : 25px">
					<button type="submit" class="btn btn-primary">
						Filter
					</button>
				</div>
			</div>
		   	{!! Form::close() !!}

		   	<?php
				$sort_array = [ 
					'name' => 'name',
					'member_number' => 'member_number',
					'email' => 'email'
				];

				if($sort){
					$index = str_replace('-', '', $sort);
					
					if($sort==$sort_array[$index])
						$sort_array[$index] = '-'.$sort_array[$index];

					else if($sort=="-".$sort_array[$index])
						$sort_array[$index]='';
				}
					
			?>

			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th style="width: 180px">
								<a href="{{ action('Admin\CustomerController@index', [									
									'sort' => $sort_array['name'],
									'active' => $active,
									'member' => $member,
									'q' => $q
								]) }}"> 
								Nama Lengkap
								<span class="glyphicon {{ sort_icon_class($sort, 'name') }}" aria-hidden="true"></span>
								</a>
							</th>
							<th style="width: 150px">
								<a href="{{ action('Admin\CustomerController@index', [									
									'sort' => $sort_array['member_number'],
									'active' => $active,
									'member' => $member,
									'q' => $q
								]) }}"> 
								Nomor Member
								<span class="glyphicon {{ sort_icon_class($sort, 'member_number') }}" aria-hidden="true"></span>
								</a>
							</th>
							<th>
								<a href="{{ action('Admin\CustomerController@index', [									
									'sort' => $sort_array['email'],
									'active' => $active,
									'member' => $member,
									'q' => $q
								]) }}"> 
								Email
								<span class="glyphicon {{ sort_icon_class($sort, 'email') }}" aria-hidden="true"></span>
								</a>
							</th>
							<th>Alamat</th>
							<th>Telepon</th>
							<th>Status</th>
							<th>Aksi</th>
							</tr>
					</thead>
					<tbody>
						@foreach ($customers as $key => $customer)
							<tr>
								<td>{{ $key+1 }}</td>
								<td>{{ $customer->full_name }}</td>
								<td>{{ $customer->member_number ? $customer->member_number : '-' }}</td>
								<td>{{ $customer->email ? $customer->email : '-' }}</td>
								<td>{{ $customer->address ? $customer->address : '-' }}</td>
								<td>{{ $customer->phone }}</td>
								<td>
									@if ($customer->is_active)
										Aktif
									@else
										Non Aktif
									@endif
								</td>
								<td>
									<a href="{{ action('Admin\CustomerController@edit', [$customer->id]) }}" 
										class="btn btn-primary btn-xs">
										Ubah
									</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="pull-right">
				{!! $customers->appends(Request::except('page'))->render() !!}
			</div>
		</div>
	</div>
@endsection