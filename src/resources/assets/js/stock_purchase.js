$(function(){
	var purchase_items = JSON.parse($('#purchase-items').val());
	var purchase_shipping_cost_account = JSON.parse($('#shipping_accounts').val());
			
		function render() 
		{
			getIndex();
			$('#purchase-items').val(JSON.stringify(purchase_items));

			var tmpl = $('#purchase-item-table').html();
			Mustache.parse(tmpl);
			var data = { item : purchase_items };
			var html = Mustache.render(tmpl, attachHelper(data));
			$('#tbody-purchase-item').html(html);
			
			bind();
			hitungTotalSemua();
			CreatePicklist('item', '/item/list?');

			$('#cash_label').hide();
			$('#credit_label').hide();
			$('#cash_account').hide();
		}

		function attachHelper(data){
			data.formatMoney = function() {
				return function(val, render) {
					val = render(val);
					var i = parseFloat(val);
					return i.formatMoney();
				}
			};

			return data;
		}

		function bind()
		{
			$('#AddItemButton').on('click', tambahItem);
			$('.btn-delete-item').on('click', deleteItem);
			$('.btn-edit-item').on('click', editItem);
			$('.btn-save-item').on('click', saveItem);
			$('.btn-cancel-item').on('click', cancelEdit);

			$('.input-price').on('keyup', hitungTotalItem);
			$('.input-qty').on('keyup', hitungTotalItem);
			$('.input-discount').on('keyup', hitungTotalItem);
			$('.input-item').on('keyup', hitungTotalItem);
			$('.input-tax').on('keyup', hitungTotalItem);

			$('.input-all').on('keyup', hitungTotalSemua);
			$('#total_paid').on('keyup', hitungKreditOrCash);

			$('.input-new').keypress(function(e) {
				if (e.keyCode == 13) {
					e.preventDefault();
					tambahItem();
				}
			});

			$('.input-edit').keypress(function(e) {
				var i = $(this).data('id');

				if (e.keyCode == 13) {
					e.preventDefault();
					$('#simpan_' + i).click();
				}
			});
		}

		function getIndex() 
		{
			for (idx in purchase_items) {
				purchase_items[idx]['_id'] = idx;
				purchase_items[idx]['no'] = parseInt(idx) + 1;
			}
		}

		function tambahItem() 
		{
			var item_name = $('#itemName').val();
			var item_id = $('#itemId').val();
			var harga = $('#harga').val();
			var qty = $('#qty').val();
			var tax = $('#tax').val();
			var discount = $('#discount').val();
			var total = $('#total').val();

			if(discount == '')
				discount = 0;

			var input = {
				'item_id' : item_id,
				'item_name' : item_name,
				'price' : harga,
				'qty' : qty,
				'tax' : tax,
				'discount' : discount,
				'total' : total,
				'tcop' : Number(harga) - Number(discount),
			};

			var diff = checkItem(item_id, harga);
			if (!diff) {
				$('#errorInput').removeClass('hidden');
				return;
			}
			
			if (harga >= 0 && qty >= 0 && discount >= 0 && total >= 0 && item_name && item_id)
				purchase_items.push(input);

			render();
		}

		function checkItem(item_id, price, index)
		{
			for (var i in purchase_items) {
				var item = purchase_items[i];
				if (i == index)
					continue;

				if (item.item_id == item_id)
					return false;
			}

			return true;
		}

		function deleteItem() 
		{
			var i = parseInt($(this).data('id'), 10);
			
			purchase_items.splice(i, 1);
			render();
		}

		function editItem() 
		{
			var i = parseInt($(this).data('id'), 10);
			
			//$('#item_' + i).addClass('hidden');
			$('#price_' + i).addClass('hidden');
			$('#qty_' + i).addClass('hidden');
			$('#tax_' + i).addClass('hidden');
			$('#discount_' + i).addClass('hidden');
			$('#total_' + i).addClass('hidden');
			
			$('#edit_' + i).addClass('hidden');
			$('#delete_' + i).addClass('hidden');
			
			//$('#itemInputName_' + i).removeClass('hidden');
			$('#priceInput_' + i).removeClass('hidden');
			$('#qtyInput_' + i).removeClass('hidden');
			$('#taxInput_' + i).removeClass('hidden');
			$('#discountInput_' + i).removeClass('hidden');
			$('#totalInput_' + i).removeClass('hidden');			

			$('#simpan_' + i).removeClass('hidden');
			$('#cancel_' + i).removeClass('hidden');
		}

		function cancelEdit() 
		{
			var i = parseInt($(this).data('id'), 10);
			
			//$('#item_' + i).removeClass('hidden');
			$('#price_' + i).removeClass('hidden');
			$('#qty_' + i).removeClass('hidden');
			$('#tax_' + i).removeClass('hidden');
			$('#discount_' + i).removeClass('hidden');
			$('#total_' + i).removeClass('hidden');
			
			$('#edit_' + i).removeClass('hidden');
			$('#delete_' + i).removeClass('hidden');
			
			//$('#itemInputName_' + i).addClass('hidden');
			$('#priceInput_' + i).addClass('hidden');
			$('#qtyInput_' + i).addClass('hidden');
			$('#taxInput_' + i).addClass('hidden');
			$('#discountInput_' + i).addClass('hidden');
			$('#totalInput_' + i).addClass('hidden');			

			$('#simpan_' + i).addClass('hidden');
			$('#cancel_' + i).addClass('hidden');
			$('#errorInput_' + i).addClass('hidden');
		}

		function saveItem() 
		{
			var i = parseInt($(this).data('id'), 10);
			
			var item_id = $('#itemInputId_' + i).val();
			var price = $('#priceInput_' + i).val();
			var qty = $('#qtyInput_' + i).val();
			var tax = $('#taxInput_' + i).val();
			var discount = $('#discountInput_' + i).val();
			var total = $('#totalInput_' + i).val();

			var diff = checkItem(item_id, price, i);
			if (!diff) {
				$('#errorInput').addClass('hidden');
				$('#errorInput_' + i).removeClass('hidden');
				return;
			}

			if (price >= 0 && qty >= 0 && discount >= 0 && total >= 0) {
				purchase_items[i].price = price;
				purchase_items[i].qty = qty;
				purchase_items[i].tax = tax;
				purchase_items[i].discount = discount;
				purchase_items[i].total = total;
				purchase_items[i].tcop = Number(price) - Number(discount) + Number(tax);
			}

			render();
		}

		function hitungTotalItem(){
			var i = parseInt($(this).data('id'), 10);
			
			if (isNaN(i)){
				var harga = $('#harga').val();
				var qty = $('#qty').val();
				var tax = $('#tax').val();
				var discount = $('#discount').val();
				var total = (harga*qty)-(discount*qty)+(tax*qty);
				
				$('#total').val(total);

			}
			else
			{
				var price = $('#priceInput_' + i).val();
				var qty = $('#qtyInput_' + i).val();
				var tax = $('#taxInput_' + i).val();
				var discount = $('#discountInput_' + i).val();
				var total = (price*qty)-(discount*qty)+(tax*qty);
				$('#totalInput_' + i).val(total);
			}

			hitungTotalAllItem();
		}

		function hitungTotalAllItem(){
			var total_all_item = 0;
			for (i in purchase_items) {
				total_all_item = total_all_item + Number(purchase_items[i].total);
			}

			$("#totalAllItemCost").val(total_all_item);
		}

		function hitungTotalSemua(){
			hitungTotalAllItem();
			var total_item = 0;
			var balance = 0;

			for (i in purchase_items) {
				total_item = total_item + Number(purchase_items[i].total);
			}

			var diskon_total = 0;
			var biaya_kirim = 0;
			var dynamic_costs = 0;

			for(x in purchase_shipping_cost_account){
				if(purchase_shipping_cost_account[x].type == '-'){
					dynamic_costs = dynamic_costs - Number($('#biaya-kirim-' + x).val());
					if(x == 0){
						balance = Number($("#totalAllItemCost").val()) - Number($('#biaya-kirim-' + x).val());
					}else{
						balance = Number($('#balance-' + (x - 1)).val()) - Number($('#biaya-kirim-' + x).val());
					}
				}else if(purchase_shipping_cost_account[x].type == '+'){
					dynamic_costs = dynamic_costs + Number($('#biaya-kirim-' + x).val());
					if(x == 0){
						balance = Number($("#totalAllItemCost").val()) + Number($('#biaya-kirim-' + x).val());
					}else{
						balance = Number($('#balance-' + (x - 1)).val()) + Number($('#biaya-kirim-' + x).val());
					}
				}
				$("#balance-" + x).val(balance);
			}
			
			var total_semua = Number(total_item) - Number(diskon_total) + Number(biaya_kirim) + Number(dynamic_costs);
			
			$('#total-semua').val(total_semua);
			hitungKreditOrCash();
		}

		function hitungKreditOrCash(){
			var a = parseInt($('#total-semua').val());
			var b = parseInt($('#total_paid').val());

			if(isNaN(b)){
				showCreditLabel();
			}else{
				if(!isNaN(a)){
					if(b == a){
						showCashLabel();
					}else if(b < a){
						showCreditLabel();
					}else if(b > a){
						showCashLabel();
						$('#total_paid').val(a);
					}
				}else{
					showCreditLabel();
				}
				
			}
			console.log(a);
		}

		function showCashLabel(){
			$('#credit_label').hide();
			$('#cash_label').show();
			$('#cash_account').show();
		}

		function showCreditLabel(){
			$('#credit_label').show();
			$('#cash_label').hide();
			$('#cash_account').hide();
		}

		render();
});