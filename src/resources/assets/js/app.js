// set global moustache tags
Mustache.tags = ['{%', '%}'];

//set check for active radio button
$('.btn input:radio').on('change', function() {
	$('.btn input:radio').prop('checked', false);
	$(this).prop('checked', true);
});

//bootsrap datepicker
$('input.input-date').datepicker({
	format: "dd/mm/yyyy",
	autoclose: true,
	todayHighlight: true
});

function parseInputInt(obj) 
{
	var num = parseInt(obj.val(), 10);
	if (isNaN(num))
		num = 0;
	obj.val(num);
	return num;
}

function parse(obj)
{
	if (obj == undefined)
		return [];

	return JSON.parse(obj);
}

function parseDecimal(obj)
{
	var num = obj.val().replace(',', '.');
	if (isNaN(num))
		num = parseFloat(num);

	if (isNaN(num))
		num = 0;

	obj.val(num);
	return num;
}