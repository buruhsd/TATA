$(function() {
	var count = parseInt($('#countPay').val(), 10);
	var all_pay = parseInputInt($('#allPay'));
	var error = false;
	var pay_id = $('#payId').val();

	CheckSubmitBarcode();

	function calculate()
	{
		var subtotal = parseInt($('#subtotal').data('total'), 10);
		var disc_amount = $('#inputDisc').val().replace(',', '.');
		var disc_type = $('#inputDiscType option:selected').val();
		var total, percent, rounding;

		if (isNaN(disc_amount))
			$('#buttonSubmit').attr('disabled', true);
		else
			$('#buttonSubmit').removeAttr('disabled');

		$('#inputDisc').val(disc_amount);

		if (disc_type == 'IDR') {
			total = subtotal - disc_amount;
			percent = disc_amount / subtotal * 100;

			$('#labelDisc').html(percent.toFixed(2) + '%');
			//$('#labelTotalDiskon').html('Rp' + total.formatMoney());
		} else {
			//console.log(disc_amount);
			disc_amount = parseFloat(disc_amount).toFixed(2);
			disc_amount = Math.round(subtotal * disc_amount / 100);
			//disc_amount = Math.round(disc_amount/100)*100;
			total = subtotal - disc_amount;
			//total = Math.round(total/100)*100;

			$('#labelDisc').html('Rp' + disc_amount.formatMoney());
			
		}
		
		$('#labelSubtotal').html('Rp' + subtotal.formatMoney());
		$('#labelTotalDiskon').html('Rp' + total.formatMoney());
		var bulat = Math.ceil(total/100)*100;
		rounding = bulat - total;
		$('#labelRounding').html('Rp' + rounding.formatMoney());	
		var total_bulat = total+rounding;	
		$('#labelGrand').html('Rp' + total_bulat.formatMoney());

		total -= all_pay;
		total = total + rounding;
		$('#labelTotal').html('Rp' + total.formatMoney());
		$('#labelAllPay').html('Rp' + all_pay.formatMoney());
		$('#totalPay').val(total);
		calcChange();
	}

	function calcChange()
	{
		var pay = parseInputInt($('#inputPayAmount'));
		var total = parseInt($('#totalPay').val(), 10);
		var change = pay-total;
		error = false;

		if (change < 0) {
			change = change * -1;
			$('#labelChange').html('Kurang');
			$('#labelChange').attr('style', 'color: red;');
			$('#inputChange').attr('style', 'color: red;');

			if (pay <= 0)
				error = 'Input pembayaran tidak boleh minus ' + (count > 0 ? 'atau 0' : '') ;

		} else {
			$('#labelChange').html('Kembali');
			$('#labelChange').removeAttr('style');
			$('#inputChange').removeAttr('style');
		}

		if (pay == 0 && count == 0)
			error = false;

		$('#inputChange').html('Rp' + change.formatMoney());

		if (error)
			$('#inputPayAmount').attr('title', error);
	}

	function calcDiscProd()
	{
		var i = $(this).data('id');
		var disc_type = $('#discProdType_' + i).val();
		var disc_amount = $('#inputDiscProd_' + i).val().replace(',', '.');
		var subtotal = parseInt($('#subtotalBefore_' + i).data('subtotal'), 10);

		if (isNaN(disc_amount))
			$('#btnSaveDisc_' + i).attr('disabled', true);
		else
			$('#btnSaveDisc_' + i).removeAttr('disabled');

		$('#inputDiscProd_' + i).val(disc_amount);

		if (disc_type == 'IDR') {
			total = subtotal - disc_amount;
			percent = disc_amount / subtotal * 100;

			$('#labelDiscProd_' + i).html(percent.toFixed(2) + '%');
		} else {
			disc_amount = parseFloat(disc_amount).toFixed(2);
			disc_amount = Math.round(subtotal * disc_amount / 100);
			//disc_amount = Math.round(disc_amount/100)*100;
			total = subtotal - disc_amount;
			//total = Math.round(total/100)*100;

			$('#labelDiscProd_' + i).html('Rp' + disc_amount.formatMoney());
		}

		$('#subtotalAfter_' + i).html('Rp' + total.formatMoney());
	}

	$('#inputPayAmount').on('keyup', calcChange);
	$('#inputDisc').on('keyup', calculate);
	$('#inputDiscType').on('change', calculate);

	$('#inputPayAmount').on('keypress', function (e) {
		if (e.keyCode == 13) {
			e.preventDefault();
			$('#formPayment').submit();
		}
	});

	$('.input-disc-amount').on('keyup', calcDiscProd);
	$('.input-disc-type').on('change', calcDiscProd);
	$('.btn-discount').on('click', calcDiscProd);

	$('#formPayment').on('submit', function() {
		if (isNaN($('#inputDisc').val()))
			return false;

		if (error) {
			$('#inputPayAmount').tooltip('show');
			$('#payment').addClass('has-error');
			return false;
		} else {
			$('#inputPayAmount').tooltip('hide');
			$('#payment').removeClass('has-error');
		}
	});

	$('#page').on('keyup', function() {
		parseDecimal($(this));
	});

	$('#copy').on('keyup', function() {
		parseInputInt($(this));
	});

	if (pay_id) {
		var win = window.open('/cashier/' + pay_id + '/bill', '_blank');
		if (win)
  			win.focus();
	}

	calculate();
});