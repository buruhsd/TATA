$(function() {
	var items = parse($('#inputItem').val());
	var sides = parse($('#selectSide').val());
	var all_options = parse($('#selectOption').val());

	CreatePicklist('customer', '/admin/customer/list?');
	CreatePicklist('product', '/admin/product/list?');

	function renderItem()
	{
		if (items.length == 0) {
			$('#listItem').addClass('hidden');
			return;
		}

		var total = 0;
		for (var i in items) {
			items[i]['no'] = parseInt(i, 10) + 1;
			items[i]['_id'] = i;
			total += items[i]['total'];
		}

		$('#listItem').removeClass('hidden');
		$('#inputItem').val(JSON.stringify(items));

		var data = { item : items }
		var tmpl = $('#itemTmpl').html();

		Mustache.parse(tmpl);
		var html = Mustache.render(tmpl, attachHelper(data));
		$('#itemTable').html(html);

		$('#subtotal').data('total', total);
		$('#subtotal').html('<b>Rp' + total.formatMoney() + '</b>');

		$('#inputDiscType').trigger('change');

		$('.btn-delete-item').on('click', remove);
		$('.btn-edit-item').on('click', edit);
	}	

	function attachHelper(data)
	{
		data.formatMoney = function() {
			return function(val, render) {
				val = render(val);
				var i = parseFloat(val);
				return i.formatMoney();
			}
		};
		return data;
	}

	function bind()
	{
		$('#AddItemButton').on('click', add);
		$('#CancelItemButton').on('click', cancel);
		$('#SaveItemButton').on('click', save);
	
		$('#productId').on('change', function () {
			render();
		});
	}

	function render()
	{
		var id = $('#productId').val();

		if (!id) {
			$('#side').attr('disabled', true);
			return;
		}

		$('#side').removeAttr('disabled');
		$('#listOptions').addClass('hidden');

		var tmpl = $('#tmplSides').html();
		Mustache.parse(tmpl);
		var html = Mustache.render(tmpl, {data : sides[id]});
		$('#side').html(html);

		var options = all_options[id];
		if (options.length > 0)
			$('#listOptions').removeClass('hidden');

		var tmpl = $('#tmplOptions').html();
		Mustache.parse(tmpl);
		var html = Mustache.render(tmpl, {data : all_options[id]});
		$('#options').html(html);
	}

	function compile()
	{
		var p = [];
		$('.inputOption').each(function() {
			var input = {
				'option_name' : $(this).data('name'),
				'optname_id' : $(this).val(),
				'optname_name' : $('option:selected', this).html(),
			};
			p.push(input);
		});

		return p;
	}

	function add()
	{
		var id = $('#productId').val();
		var name = $('#productName').val();
		var side = $('#side option:selected').val();
		var page = parseDecimal($('#page'));
		var copy = parseInputInt($('#copy'));
		var filename = $('#fileName').val();

		if (!id || !name || page <= 0 || copy <= 0)
			return;

		var option = compile();

		var input = {
			'id' : id,
			'name' : name.toUpperCase(),
			'filename' : filename,
			'side' : side,
			'page' : page,
			'copy' : copy,
			'option': option,
		};

		items.push(input);
		getPrice(JSON.stringify(items));
	}

	function remove()
	{
		var i = $(this).data('id');

		items.splice(i, 1);
		getPrice(JSON.stringify(items));
	}

	function edit()
	{
		var i = $(this).data('id');

		$('.btn-edit-item').addClass('hidden');
		$('.btn-delete-item').addClass('hidden');
		$('.edit-state-button').removeClass('hidden');
		$('#AddItemButton').addClass('hidden');
		$('#PayButton').addClass('hidden');
		$('#editTitle').removeClass('hidden');

		$('#itemRow_' + i).addClass('active');
		$('#editTitle').html('Edit Item #' + items[i].no);
		$('.edit-state-button').data('id', i);

		$('#productName').val(items[i].name);
		$('#productId').val(items[i].id);

		render();

		$('#side').val(items[i].side);
		$('#page').val(items[i].page);
		$('#copy').val(items[i].copy);
		$('#fileName').val(items[i].filename);

		$('.inputOption').each(function(index) {
			var option = items[i].option;
			var selected = option[index].optname_id;

			$(this).find('[value=' + selected +']').attr('selected', true);
		});

		$('.detail').find('input:text').unbind();
		$('.detail').find('input:text').on('keypress', function (e) {
			if (e.keyCode == 13) {
				e.preventDefault();
				$('#SaveItemButton').click();
			}
		});
	}

	function cancel()
	{
		var i = $('.edit-state-button').data('id');

		$('.btn-edit-item').removeClass('hidden');
		$('.btn-delete-item').removeClass('hidden');
		$('.edit-state-button').addClass('hidden');

		$('#AddItemButton').removeClass('hidden');
		$('#PayButton').removeClass('hidden');
		$('#editTitle').addClass('hidden');
		$('#itemRow_' + i).removeClass('active');

		$('.detail').find('input:text').unbind();
		$('.detail').find('input:text').on('keypress', function (e) {
			if (e.keyCode == 13) {
				e.preventDefault();
				add();
			}
		});

		clean();
	}

	function save()
	{
		var i = $('.edit-state-button').data('id');

		var id = $('#productId').val();
		var name = $('#productName').val();
		var side = $('#side option:selected').val();
		var page = parseDecimal($('#page'));
		var copy = parseInputInt($('#copy'));
		var filename = $('#fileName').val();

		if (!id || !name || page <= 0 || copy <= 0)
			return;

		var option = compile();

		items[i].id = id;
		items[i].name = name.toUpperCase();
		items[i].side = side;
		items[i].page = page;
		items[i].copy = copy;
		items[i].option = option;
		items[i].filename = filename;

		cancel();
		getPrice(JSON.stringify(items));
	}

	function getPrice(data)
	{
		if (items.length == 0) {
			renderItem();
			return;
		}

		$('#shadeLoading').removeClass('hidden');
		$('#shadeLoading').addClass('modal-backdrop in');

		$.ajax ({
			url: '/cashier/price?data=' + data,
		}).done(function(data) {
			items = data;
			console.log(data);

			clean();
			$('#shadeLoading').addClass('hidden');
			$('#shadeLoading').removeClass('modal-backdrop in');

			renderItem();
		});
	}

	function clean()
	{
		$('#copy').val('');
		$('#page').val('');
		$('#side').val('');
		$('#productId').val('');
		$('#productName').val('');
		$('#fileName').val('');
		$('#listOptions').addClass('hidden');
		$('#productButton').focus();

		render();
	}

	function checkCust()
	{
		var cust_id = $('#customerId').val();

		if (cust_id)
			$('#customerName').attr('readonly', true);
		else
			$('#customerName').removeAttr('readonly');
	}

	$('.detail').find('input:text').on('keypress', function (e) {
		if (e.keyCode == 13) {
			e.preventDefault();
			add();
		}
	});

	$('#PayButton').on('click', function() {
		var cust = $('#customerName').val();
		var cust_id = $('#customerId').val();

		$('#inputCustomer').val(cust);
		$('#inputCustomerId').val(cust_id);
	});

	$('#customerId').on('change', checkCust);
	$('#customerButtonDel').on('click', checkCust);
	$('#customerName').removeAttr('data-toggle');

	getPrice(JSON.stringify(items));
	checkCust();
	render();
	bind();
});