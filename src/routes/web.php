<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'HomeController@index');


Auth::routes();

Route::get('/home', 'HomeController@index');
//Routing on Customer database
Route::get('/customer', 'CustomerController@index');
Route::get('/customer/add', 'CustomerController@create');
Route::post('/customer/add/store', 'CustomerController@store');
Route::get('/customer/{id}/edit/', 'CustomerController@edit');
Route::post('/customer/{id}/edit/', 'CustomerController@update');
Route::get('/customer/list/', 'CustomerController@picklist');
Route::get('/customer/{id}/detail/', 'CustomerController@show');

//Routing Purchase Order
Route::get('/purchase', 'PurchasesController@index');
Route::get('/purchase/add', 'PurchasesController@create');
Route::post('/purchase/store', 'PurchasesController@store');
Route::get('/purchase/{id}/edit/', 'PurchasesController@edit');
Route::post('/purchase/{id}/edit/', 'PurchasesController@update');
//Route::get('/purchase/destroy/{id}', 'PurchasesController@index');

//Routing Purchase Manager
Route::get('/manager', 'PurchasesController@ordermanager');
Route::get('/manager/{id}/edit', 'PurchasesController@manageorder');
Route::post('/manager/{id}/edit', 'PurchasesController@statusupdate');
Route::get('/sendmail/{id}','PurchasesController@sendmail');


Route::get('/settings', function() {
	return view('settings.index');
});

//routing finace
Route::get('/finace', 'FinaceController@index');
Route::get('/finace/{id}/manage', 'FinaceController@managefinace');
Route::post('/finace/{id}/manage', 'FinaceController@paymentupdate');


Route::get('/nojs', 'HomeController@nojs');

Route::get('/logout', 'Auth\LoginController@logout');
Auth::routes();

Route::get('/home', 'HomeController@index');
