<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;
use App\Models\Purchase;
use App\Models\Customer;
use App\Models\PurchaseOrderDetail;


class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $orders = PurchaseOrderDetail::orderBy('id', 'DESC')->paginate(5);
        return view('manager.index',compact('orders'))
        ->with('i', ($request->input('page',1) -1)* 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('purchase.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'po_number'=>'required',
            'order'=>'required'
            ]);
        Purchase::create($request->all());

        return redirect()->action('PurchasesController@index')

                        ->with('success',' created successfully');
    }


    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $purchase = Purchase::find($id);
        return view('purchase.edit',compact ('purchase'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

            public function picklist(Request $request)
    {
        $q = $request->q;
        $lists = Customer::select(DB::raw('id, name as name'))
            ->where('name', 'like', "%$q%")
            ->orderBy('name')->paginate('5');

        return view('customer._customer_list')
            ->with('lists', $lists);

    }

    public function ordermanager(Request $request)
    {
         $orderdetail = Purchase::orderBy('id', 'DESC')->paginate(5);
        return view('manager.index',compact('orderdetail'))
        ->with('i', ($request->input('page',1) -1)* 5);
    }

    public function manageorder($id)
    {
        $orderdetail = Order::find($id);
        return view('manager.edit',compact ('orderdetail'));
    }
    
    public function destroy($id)
    {
        //
    }
}
