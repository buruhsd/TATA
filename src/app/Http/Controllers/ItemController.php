<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Purchae_item;
use App\Models\Purchases;

class ItemController extends Controller
{
    //
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'item_name'=>'required',
            ]);
        Purchase::create([
            'item_name' =>$request-> input('item_name'),
            'po_id' => $request->input('po_id'),
            'quantity' => $request->input('quantity'),
            'price' => $request->input('price'),
            'note' => $request->input('note')
            ]);

        return redirect()->action('PurchasesController@index')

                        ->with('success',' created successfully');
    }

}
