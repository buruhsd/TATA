<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;
use App\Models\Purchase;
use App\Models\Customer;
use App\Models\Purchase_item;
use App\Models\PurchaseOrderDetail;
use App\Models\Status;
use App\Models\Finace;
use Carbon\Carbon;
use DB;
use Stdclass;

class FinaceController extends Controller
{
    //
    public function index(Request $request) {
    	$purchases = Purchase::orderBy('id', 'DESC')->paginate(5);
        return view('finace.index',compact('purchases'))
        ->with('i', ($request->input('page',1) -1)* 5);
    }

    public function managefinace(Request $request, $id)
    {
        $purchase = Purchase::find($id);
        
        //$orders= Purchase_item::all();
        //var_dump($purchase->orders()->get()); die();
        $orders=[];
        $no = 1;
        foreach ($purchase->Orders()->get() as $key => $order) {
            $data = new Stdclass();
            $data->_id = $order->id;
            $data->no = $no;
            $data->item_name= $order->item_name;
            $data->qty=$order->qty;
            $data->price=$order->price;
            $data->note=$order->notes;
            //$data->order= $order->purchase_items;
            # code...
            $orders[] = $data;
        }

        $purchase_items = json_encode($orders);
        $finaces = Finace::where('purchase_id', $id)->get();

       // var_dump($purchase_items); die();
        return view('finace.edit')
         ->with('purchase', $purchase)
         ->with('purchase_items', $purchase_items)
         ->with('finaces', $finaces);
    }

    public function paymentupdate(Request $request, $id)
    {
        //
        $purchase = Purchase::find($id);

        $finace = new Finace();

        $this->validate($request, [
            'payment' => 'required|integer',
           ]);

        $finace->fill([
            'payment' =>$request->payment,
            'purchase_id' => $request->purchase_id,
            'note' => $request->note,
        //    'message' => $request->message,
        //    'image' => $request->hasFile('image')
            ]);

        $finace->save();

        return redirect()->back()
            ->with('success',' created successfully');
}
    }


