<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Purchase;
use Illuminate\Support\Facades\View;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $customers = Customer::orderBy('id', 'DESC')->paginate(5);
        return view('customer.index',compact('customers'))
        ->with('i', ($request->input('page',1) -1)* 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('customer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name'=>'required',
            'email'=>'required',
            'password'=>'required',
            'company'=>'required',
            'country'=>'required']);
        Customer::create($request->all());

        return redirect()->action('CustomerController@index')

                        ->with('success',' created successfully');
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $customer=Customer::find($id);
         return view('customer.detail', compact('customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $customer = Customer::find($id);
        return view('customer.edit',compact ('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'name'=>'required',
            'email'=>'required',
            'password'=>'required',
            'company'=>'required',
            'country'=>'required']);
        Customer::find($id)->update($request->all());

        return redirect()->action('CustomerController@index')

                        ->with('success',' created successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function picklist(Request $request)
    {
    $q = $request->q;
    $lists = Customer::where('name', 'like', "%$q%")
        ->orderBy('id')->paginate('5');
    return view('customer._customer_list', ['lists' => $lists]);

    }

    public function destroy($id)
    {
        //
    }
}
