<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;
use App\Models\Purchase;
use App\Models\Customer;
use App\Models\Purchase_item;
use App\Models\PurchaseOrderDetail;
use App\Models\Status;
use Carbon\Carbon;
use Mail;
use DB;
use Stdclass;


class PurchasesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $purchases = Purchase::orderBy('id', 'DESC')->paginate(5);
        return view('purchase.index',compact('purchases'))
        ->with('i', ($request->input('page',1) -1)* 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $item = Purchase_item::orderby('id')->pluck('item_name', 'id');
        return view('purchase.create') 
            ->with('item', $item);
        
    }

    public function add()

    {
        return view('home');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $date = Carbon::createFromFormat('d/m/Y H:i', $request->date.' 00:00');

        $this->validate($request, [
            'po_number'=>'required',
            ]);

        $purchase = new Purchase();
        $this->validate($request, [
            'po_number' => 'required',
           ]);
        $purchase->fill([
            'po_number' =>$request->po_number,
            'customer_id' => $request->customer_id_id,
            'date' => $date,
            'keterangan' => $request->keterangan,
            'status' => $request->status,
            ]);
        $purchase->save();


        $purchase_items_json = json_decode($request->purchase_items);
        //var_dump($purchase_items_json);die;

        $purchase_items = array_map(function($i){
            $new = new Purchase_item();
            $new->item_name = $i->item_name;
            $new->qty = $i->qty;
            $new->price = $i->price;
            $new->notes = $i->note;
            return $new;
        }, $purchase_items_json);

        $purchase->orders()->saveMany($purchase_items);

        

        return redirect()->action('PurchasesController@index')

                        ->with('success',' created successfully');
    }


    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $Request, $id)
    {
        //
        $purchase = Purchase::find($id);

        //$orders= Purchase_item::all();
        //var_dump($purchase->orders()->get()); die();
        $orders=[];
        $no = 1;
        foreach ($purchase->Orders()->get() as $key => $order) {
            $data = new Stdclass();
            $data->_id = $order->id;
            $data->no = $no;
            $data->item_name= $order->item_name;
            $data->qty=$order->qty;
            $data->price=$order->price;
            $data->note=$order->notes;
            //$data->order= $order->purchase_items;
            # code...
            $orders[] = $data;
        }

        $purchase_items = json_encode($orders);

       // var_dump($purchase_items); die();
        return view('purchase.edit')
        ->with('purchase', $purchase)
        ->with('purchase_items', $purchase_items);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
//       $date = Carbon::createFromFormat('d/m/Y H:i', $request->date.' 00:00');

        $purchase = Purchase::find($id);

        $purchase->fill([
            'po_number' =>$request->po_number,
            'customer_id' => $request->customer_id_id,
            'date' => $request->date,
            'keterangan' => $request->keterangan,
            'status' => $request->status,
            ]);
        $purchase->save();

        //var_dump($purchase); die();

        $purchase_items_json = json_decode($request->purchase_items);

        $purchase_items = array_map(function($i){
            $new = new Purchase_item();
            $new->item_name = $i->item_name;
            $new->qty = $i->qty;
            $new->price = $i->price;
            $new->notes = $i->note;
            return $new;
        }, $purchase_items_json);

        //DB::table('purchase_items')->where('purchase_id', $id)->delete();

        Purchase_item::where('purchase_id', $id)->delete();
         $purchase->orders()->saveMany($purchase_items);

        

        return redirect()->action('PurchasesController@index')
                        ->with('success',' created successfully');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function picklist(Request $request)
    {
        $q = $request->q;
        $lists = Customer::where('name', 'like', "%$q%")
            ->orderBy('name')->paginate('5');
        return view('customer._customer_list')
            ->with('lists', $lists);

    }

    public function ordermanager(Request $request)
    {
         $purchases = Purchase::orderBy('id', 'DESC')->paginate(5);
        return view('manager.index',compact('purchases'))
        ->with('i', ($request->input('page',1) -1)* 5);
    }

    public function manageorder(Request $request, $id)
    {
        $purchase = Purchase::find($id);
        
        //$orders= Purchase_item::all();
        //var_dump($purchase->orders()->get()); die();
        $orders=[];
        $no = 1;
        foreach ($purchase->Orders()->get() as $key => $order) {
            $data = new Stdclass();
            $data->_id = $order->id;
            $data->no = $no;
            $data->item_name= $order->item_name;
            $data->qty=$order->qty;
            $data->price=$order->price;
            $data->note=$order->notes;
            //$data->order= $order->purchase_items;
            # code...
            $orders[] = $data;
        }

        $purchase_items = json_encode($orders);
        $statuses = Status::where('purchase_id', $id)->get();

       // var_dump($purchase_items); die();
        return view('manager.edit')
         ->with('purchase', $purchase)
         ->with('purchase_items', $purchase_items)
         ->with('statuses', $statuses);
    }


    public function statusupdate(Request $request, $id)
    {
        //
        $purchase = Purchase::find($id);

       // $status = new Status();

        //$status->fill([
        //    'status_name' =>$request->status_name,
        //    'purchase_id' => $request->purchase_id,
        //    'date' => $request->date,
        //    'message' => $request->message,
        //    'image' => $request->hasFile('image')
        //    ]);

        //$status->save();
        $this->validate($request, [
            'status_name' => 'required',
           'image' => 'image|max:2048',
           ]);
        $status=Status::create([
            $request->except('image'),
            'status_name' =>$request->input('status_name'),
            'purchase_id' => $request->input('purchase_id'),
            'date' => $request->input('date'),
            'message' => $request->input('message')
            ]);
            if ($request->hasFile('image')) {
            $uploaded_image = $request->file('image');

            $extension = $uploaded_image->getClientOriginalExtension();
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img';
            $uploaded_image->move($destinationPath, $filename);
            $status->image = $filename;
            //$status->sendmail();
            $status->save();

        }

       // $title = $request->input('title');
        //$content = $request->input('content');
       // $email = $request->input($purchase->customers->email);
       
        //Mail::send('manager.edit', ['title'=>$title, 'content' => $content, 'purchase' => $purchase, 'statuses' => $statuses], function($pesan) use ($purchase) {
        //    $pesan->to($purchase->customer->email, $purchase->customer->name)->subject('System TATA!');
        //    });
            
       //var_dump($status); die();
       // var_dump($status); die();
        
//        Mail::send('welcome', $data, function($pesan) {
//            $pesan->to('sang.elaang@gmail.com', $purchase->customers->name)->subject('System TATA!');
          //  });
        return redirect()->back()
            ->with('success',' created successfully');

    }

    public function sendmail(Request $request, $id)
    {
        $purchase = Purchase::find($id);
        $statuses = Status::where('purchase_id', $id)->get();
       // $purchase = Purchase::find($id);

        $title = $request->input('title');
        $content = $request->input('content');
       // $email = $request->input($purchase->customers->email);
       
        Mail::send('welcome', ['title'=>$title, 'content' => $content, 'purchase' => $purchase, 'statuses' => $statuses], function($pesan) use ($purchase) {
            $pesan->to('sang@gmail.com', '$purchase->customers->name')->subject('System TATA!');
            });
    }
    
    public function destroy($id)
    {
        //
    }
}

