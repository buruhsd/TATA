<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AuthenticatesRealm;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Log;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\Customer\CustomerListRequest;
use App\Http\Requests\Admin\Customer\CustomerCreateRequest;
use App\Http\Requests\Admin\Customer\CustomerStoreRequest;
use App\Http\Requests\Admin\Customer\CustomerEditRequest;
use App\Http\Requests\Admin\Customer\CustomerUpdateRequest;
use App\Http\Requests\Admin\Customer\CustomerDeleteRequest;

use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Cell_DataType;
use Config;
use Auth;
use DB;

class CustomerController extends Controller {

	use AuthenticatesRealm;

	public function index(CustomerListRequest $request)
	{
		$q = $request->q;
		$active = $request->active;
		$member = $request->member;
		$sort = $request->sort;
		$customers = Customer::search($q, $active);

		if ($member == 'member')
			$customers = $customers->whereNotNull('member_number')->where('member_number','<>','');
		elseif ($member == 'non-member')
			$customers = $customers->whereNull('member_number')->orWhere('member_number','=','');

		if($sort){
			if($sort == "name")
				$customers = $customers->orderBy('full_name', 'asc');
			elseif($sort == "-name")
				$customers = $customers->orderBy('full_name', 'desc');
			elseif($sort == "member_number")
				$customers = $customers->orderByRaw('(member_number = ""), member_number ASC');
			elseif($sort == "-member_number")
				$customers = $customers->orderByRaw('(member_number = ""), member_number DESC');
			elseif($sort == "email")
				$customers = $customers->orderByRaw('(email = ""), email ASC');
			elseif($sort == "-email")
				$customers = $customers->orderByRaw('(email = ""), email DESC');
		} else{
			$customers = $customers->orderBy('full_name');
		}

		$customers = $customers->paginate(10);

		return view('admin.customer_list')
			->with('customers', $customers)
			->with('member', $member)
			->with('active', $active)
			->with('q', $q)
			->with('sort', $sort);
	}

	public function create(CustomerCreateRequest $request)
	{
		$customer = new Customer();
		return view('admin.customer_create')
			->with('customer', $customer);
	}

	public function edit(CustomerEditRequest $request, $id)
	{
		$customer = Customer::findOrFail($id);
		
		return view('admin.customer_edit')
			->with('customer', $customer);
	}

	public function store(CustomerStoreRequest $request)
	{
		$customer = new Customer();
		Log::log('CREATE.SUCCESS|PELANGGAN', Auth::user(), $customer);

		$customer->fill([
			'full_name' => ucwords($request->full_name),
			'email' => $request->email,
			'address' => $request->address,
			'phone' => $request->phone,
			'is_active' => $request->is_active,
			'member_number' => $request->member_number
		]);
		
		$customer->save();
	
		return redirect()->action('Admin\CustomerController@create')
			->with('UPDATE.OK', true);
	}

	public function update(CustomerUpdateRequest $request, $id)
	{
		$customer = Customer::findOrFail($id);
		Log::log('UPDATE.SUCCESS|PELANGGAN', Auth::user(), $customer);

		$customer->fill([
			'full_name' => ucwords($request->full_name),
			'email' => $request->email,
			'address' => $request->address,
			'phone' => $request->phone,
			'is_active' => $request->is_active,
			'member_number' => $request->member_number
		]);
		$customer->save();
		
		return redirect()->action('Admin\CustomerController@edit', [$id])
			->with('UPDATE.OK', true);
	}

	public function delete(CustomerDeleteRequest $request, $id)
	{
		$customer = Customer::findOrFail($id);
		if ($customer->orders->count() == 0) {
			Log::log('DELETE.SUCCESS|PELANGGAN', Auth::user(), $customer);
			$customer->delete();

			return redirect()->action('Admin\CustomerController@index')
				->with('DELETE.OK', true);;
		}

		Log::log('UPDATE.SUCCESS|STATUS AKTIF PELANGGAN', Auth::user(), $customer);

		$customer->is_active = 0;
		$customer->save();

		return redirect()->action('Admin\CustomerController@edit', [$id])
			->with('DELETE.DEACTIVATE', true);
	}

	public function report(CustomerListRequest $request)
	{
		$customers = New Customer();
		$state = $request->state;
		$member = $request->member;

		if ($state == 'active')
			$customers = $customers->active();
		elseif ($state == 'inactive')
			$customers = $customers->where('is_active', '=', 0);

		if ($member == 'member')
			$customers = $customers->whereNotNull('member_number')->where('member_number','<>','');
		elseif ($member == 'non-member')
			$customers = $customers->whereNull('member_number')->orWhere('member_number','=','');

		$sort = $request->sort;
		if($sort){
			if($sort == 'name')
				$customers = $customers->orderBy('full_name', 'asc');
			elseif($sort == '-name')
				$customers = $customers->orderBy('full_name', 'desc');
			elseif($sort == 'no_member')
				$customers = $customers->orderByRaw('(member_number = ""), member_number ASC');
			elseif($sort == '-no_member')
				$customers = $customers->orderByRaw('(member_number = ""), member_number DESC');
			elseif($sort == 'email')
				$customers = $customers->orderByRaw('(email = ""), email ASC');
			elseif($sort == '-email')
				$customers = $customers->orderByRaw('(email = ""), email DESC');
			elseif($sort == 'state')
				$customers = $customers->orderBy('is_active', 'desc');
			elseif($sort == '-state')
				$customers = $customers->orderBy('is_active', 'asc');
		} else
			$customers = $customers->orderBy('full_name', 'asc');

		$customers = $customers->paginate(10);

		return view('admin.customer_report')
			->with('customers', $customers)
			->with('state', $state)
			->with('member', $member)
			->with('sort', $sort);
	}

	public function export(CustomerListRequest $request)
	{
		$customers = New Customer();
		$state = $request->state;
		$member = $request->member;

		if ($state == 'active')
			$customers = $customers->active();
		elseif ($state == 'inactive')
			$customers = $customers->where('is_active', '=', 0);

		if ($member == 'member')
			$customers = $customers->whereNotNull('member_number')->where('member_number','<>','');
		elseif ($member == 'non-member')
			$customers = $customers->whereNull('member_number')->orWhere('member_number','=','');

		$sort = $request->sort;
		if($sort){
			if($sort == 'name')
				$customers = $customers->orderBy('full_name', 'asc');
			elseif($sort == '-name')
				$customers = $customers->orderBy('full_name', 'desc');
			elseif($sort == 'no_member')
				$customers = $customers->orderByRaw('(member_number = ""), member_number ASC');
			elseif($sort == '-no_member')
				$customers = $customers->orderByRaw('(member_number = ""), member_number DESC');
			elseif($sort == 'email')
				$customers = $customers->orderByRaw('(email = ""), email ASC');
			elseif($sort == '-email')
				$customers = $customers->orderByRaw('(email = ""), email DESC');
			elseif($sort == 'state')
				$customers = $customers->orderBy('is_active', 'desc');
			elseif($sort == '-state')
				$customers = $customers->orderBy('is_active', 'asc');
		} else
			$customers = $customers->orderBy('full_name', 'asc');

	    // Create new PHPExcel object
	    $objPHPExcel = new PHPExcel();

	    // Add header
	    $objPHPExcel->getActiveSheet()->getStyle('A1:G3')->getFont()->setBold(true);
	    $objPHPExcel->getActiveSheet()->getStyle('A1:G2')
	     	->getAlignment()
    		->setHorizontal('center');

	    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:G1');
	    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:G2');

	    $desc = null;
	    if ($member == 'member')
	    	$desc = '(Member)';
	    elseif ($member == 'non-member')
	    	$desc = '(Bukan Member)';

	    $objPHPExcel->setActiveSheetIndex(0)
	        ->setCellValue('A1', 'Laporan Pelanggan ' . $desc)
	        ->setCellValue('A3', '#')
	        ->setCellValue('B3', 'Nama Lengkap')
	        ->setCellValue('C3', 'Nomor Member')
	        ->setCellValue('D3', 'Email')
	        ->setCellValue('E3', 'Alamat')
	        ->setCellValue('F3', 'Telepon')
	        ->setCellValue('G3', 'Status');

	    //Add data from db
	    foreach ($customers->get() as $key => $customer) {
	    	$row = $key + 4;
	    	$objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . $row, $customer->phone, PHPExcel_Cell_DataType::TYPE_STRING);
	    	$objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $row, $customer->member_number, PHPExcel_Cell_DataType::TYPE_STRING);

	    	$sheet = $objPHPExcel->setActiveSheetIndex(0)
		        ->setCellValue('A' . $row, $key + 1)
		        ->setCellValue('B' . $row, $customer->full_name)
		        ->setCellValue('D' . $row, $customer->email)
		        ->setCellValue('E' . $row, $customer->address)
		        ->setCellValue('G' . $row, $customer->is_active ? 'Aktif' : 'Non Aktif');
	    } 

	    // Set autosize
	    for($col = 'A'; $col !== 'H'; $col++) {
		    $objPHPExcel->getActiveSheet()
		        ->getColumnDimension($col)
		        ->setAutoSize(true);
		}

	   // Rename worksheet
	    $objPHPExcel->getActiveSheet()->setTitle('Laporan Pelanggan');

	    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
	    $objPHPExcel->setActiveSheetIndex(0);

	    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

	    $path = Config::get('storage.customer');
	    $file = str_random(32).'.xlsx';
	    $objWriter->save($path . '/' . $file);
   
    	$resp = response()->download($path . '/' . $file);
    	$resp->headers->set('Content-Disposition', 'attachment;filename="Laporan Pelanggan.xlsx"');
    	$resp->headers->set('Content-Type', 'application/vnd.ms-excel');
    	$resp->headers->set('X-Content-Type-Options', 'nosniff');    	
    	return $resp;
	}

	public function picklist(Request $request)
	{
		$q = $request->q;
		$lists = Customer::select(DB::raw('id, full_name as name'))
			->where('full_name', 'like', "%$q%")
			->orderBy('full_name')->paginate('5');

		return view('admin._customer_list')
			->with('lists', $lists);

	}
}