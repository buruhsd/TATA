<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Purchase_item extends Model
{
    //
    protected $fillable=['name_item', 'purchase_id', 'quantity', 'price', 'note'];
    protected $dates = [
    'date',
    'created_at',
    'updated_at',
  ];

    public function purchase(){
    	return $this->belongsTo('App\Models\Purchases', 'purchase_id');
    }
}
