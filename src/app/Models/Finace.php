<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Finace extends Model
{
    //
    protected $fillable = ['payment', 'purchase_id', 'note'];
    protected $dates = [
    'date',
    'created_at',
    'updated_at',
  ];

    public function purchase(){
    	return $this->belongsTo('App\Models\Purchases', 'purchase_id');

    }
}
