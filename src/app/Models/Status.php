<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Customers;
//use App\Models\Status;

class Status extends Model
{
    //
    protected $fillable = ['status_name', 'purchase_id', 'message', 'date', 'image'];
    protected $dates = [
    'date',
    'created_at',
    'updated_at',
  ];

    public function purchase(){

    	  return $this->belongsTo('App\Models\Purchases', 'purchase_id');
    }

}
