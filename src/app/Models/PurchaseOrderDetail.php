<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrderDetail extends Model
{
    //
    protected $fillable=['po_number', 'customer_id', 'order', 'status'];

    public function purchase(){
    	return $this->belongsTo('App\Models\Purchases', 'customer_id');
    }

}
