<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
    //protected $guarded = ['id'];
    protected $fillable=['full_name','email', 'password', 'company', 'country'];
    protected $dates = [
    'date',
    'created_at',
    'updated_at',
  ];

   // public function purchases(){
   // 	return $this->hasMany('App\Models\Purchase');
   // }
    public function purchases(){
   		return $this->hasMany('App\Models\Purchase');
      }
}
