<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    //
    protected $fillable=['po_number','customer_id' , 'customer_name', 'date', 'keterangan', 'status'];
    protected $dates = [
    'date',
    'created_at',
    'updated_at',
  ];

   // public function customers(){
   // 	return $this->belongsTo('App\Models\Customer');
   // }
   // public function customers(){
    //	return $this->belongsTo('App\Models\Customer', 'customer_id');
   // }
    public function orders(){
   		return $this->hasMany('App\Models\Purchase_item');
      }

    public function status(){
      return $this->hasMany('App\Models\Status');
    }

    public function finace(){
      return $this->hasMany('App\Models\Finace');
    }
}
