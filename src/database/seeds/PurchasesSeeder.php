<?php

use Illuminate\Database\Seeder;
use App\Models\Customer;
use App\Models\Purchase;

class PurchasesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $customer1 = Customer::create(['name'=> 'Muhammad Al Jawad', 'email'=>'sang.elaang@gmail.com', 'password'=>bcrypt('admin'), 'company'=>'PT abc', 'country'=>'indonesia']);
        $customer2 = Customer::create(['name'=> 'Muhammad', 'email'=>'sang@gmail.com', 'password'=>bcrypt('admin'), 'company'=>'PT abc', 'country'=>'indonesia']);

        //$Purchase1 = Purchase::create(['po_number'=>'123', 'customer_id'=>$customer1->id, 'keterangan'=>'kursi 100', 'date' => '01-12-31 23:51:39', 'status' => 'Status PO']);

    }
}
