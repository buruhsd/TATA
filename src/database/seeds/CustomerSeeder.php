<?php

use Illuminate\Database\Seeder;
use App\Models\Customer;
use Carbon\Carbon;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $customer = new Customer();
        $customer->name= "Muhammad Al Jawad";
        $customer->company= "PT A";
        $customer->country= "Indonesia";
        $customer->date= Carbon::now();
        $customer->save();
    }
}
