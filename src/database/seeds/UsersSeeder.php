<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;


class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //make role superadmin
        $adminRole = new Role();
        $adminRole->name = 'admin';
        $adminRole->display_name = 'SuperAdmin';
        $adminRole->save();

        //make role admin keuangan
        $payadminRole = new Role();
        $payadminRole->name = 'payadmin';
        $payadminRole->display_name = 'AdminKeuangan';
        $payadminRole->save();

        //make role marketing
        $marketingRole = new Role();
        $marketingRole->name = 'marketing';
        $marketingRole->display_name = 'Marketing';
        $marketingRole->save();

        //Make role Customer
        $customerRole = new Role();
        $customerRole->name = 'customer';
        $customerRole->display_name = 'Customer';
        $customerRole->save();

        //Make sample user for superadmin
        $admin = new User();
        $admin->name = 'Admin Tata';
        $admin->email = 'admin@gmail.com';
        $admin->password = bcrypt('admin');
        $admin->save();
        $admin->attachRole($adminRole);

        //Make sample user for admin keuangan
        $payadmin = new User();
        $payadmin->name = 'Admin Keuangan Tata';
        $payadmin->email = 'adminkeuangan@gmail.com';
        $payadmin->password = bcrypt('admin');
        $payadmin->save();
        $payadmin->attachRole($payadminRole);

        //Make sample user for Marketing
        $marketing = new User();
        $marketing->name = 'Marketing';
        $marketing->email = 'marketing@gmail.com';
        $marketing->password = bcrypt('marketing');
        $marketing->save();
        $marketing->attachRole($marketingRole);

        //Make sample user for Customer
        $customer = new User();
        $customer->name = 'Customer';
        $customer->email = 'customer@gmail.com';
        $customer->password = bcrypt('customer');
        $customer->save();
        $customer->attachRole($customerRole);

       
    }
}
