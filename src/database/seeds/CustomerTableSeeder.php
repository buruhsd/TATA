<?php

use Illuminate\Database\Seeder;
use App\Models\Customer;
use Faker\Factory as Faker;

class CustomerTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		 $faker = Faker::create();

		 for ($i = 0; $i < 100; $i++) {
		 	Customer::create([
                'full_name' => $faker->name,
                'email' => $faker->email,
                'password' => $faker->password,
                'company' => $faker->company,
                'country' => $faker->country,
                'is_active' => $faker->randomElement([0, 1]),
            ]);
		 }
	}

}