<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(UsersSeeder::class);
      //  $this->call(CustomerSeeder::class);
       // $this->call(PurchaseSeeder::class);
        $this->call(PurchasesSeeder::class);
       // $this->call(CustomerSeeder::class);
    }
}
